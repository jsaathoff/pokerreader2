#-------------------------------------------------
#
# Project created by QtCreator 2012-12-11T18:34:22
#
#-------------------------------------------------
include(../PokerReaderQTConf.pri)

QT       -= gui
TEMPLATE = lib
DESTDIR = $${PRLIBDIR}
TARGET = ibpplib

CONFIG += staticlib

DEFINES += IBPP_LIBRARY

#CONFIG += debug_and_release


CONFIG(debug, debug|release) {
        mac: TARGET = $$join(TARGET,,,_debug)
        win32: TARGET = $$join(TARGET,,,d)
 }

win32: DEFINES += IBPP_WINDOWS

SOURCES += \
    user.cpp \
    transaction.cpp \
    time.cpp \
    statement.cpp \
    service.cpp \
    row.cpp \
    exception.cpp \
    events.cpp \
    dbkey.cpp \
    date.cpp \
    database.cpp \
    blob.cpp \
    array.cpp \
    _tpb.cpp \
    _spb.cpp \
    _rb.cpp \
    _ibs.cpp \
    _ibpp.cpp \
    _dpb.cpp

HEADERS += \
    ibpp.h \
    iberror.h \
    ibase.h \
    _ibpp.h

