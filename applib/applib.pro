#-------------------------------------------------
#
# Project created by QtCreator 2011-08-04T11:52:58
#
#-------------------------------------------------
include(../pokerreaderqtconf.pri)

QT       -= gui
TEMPLATE = lib
DESTDIR = $${PRLIBDIR}
TARGET = applib


CONFIG += staticlib

DEFINES += APPLIB_LIBRARY

DEFINES += BOOST_ALL_NO_LIB

CONFIG += debug_and_release

CONFIG(debug, debug|release) {
	mac: TARGET = $$join(TARGET,,,_debug)
	win32: TARGET = $$join(TARGET,,,d)
 }

INCLUDEPATH += $${BOOST}

win32: DEFINES += IBPP_WINDOWS

SOURCES += applibrary.cpp \
		applicationpaths.cpp \
		settings.cpp \
	datatypes/pokerdefinitions.cpp \
	parser/baseparsertext.cpp \
	parser/handimporter.cpp \
	parser/pokerstarsparser.cpp \
	datatypes/hand.cpp \
	parser/importcontainer.cpp \
    parser/baseparser.cpp \
    parser/partypokerparser.cpp \
    parser/fulltiltparser.cpp \
    converter.cpp \
    encodingdetector.cpp
	
HEADERS += applibrary.h\
				AppLib_global.h \
		applicationpaths.h \
		settings.h \
		definitions.h \
	datatypes/pokerdefinitions.h \
	parser/baseparsertext.h \
	parser/handimporter.h \
	parser/pokerstarsparser.h \
	datatypes/hand.h \
	parser/importcontainer.h \
    parser/baseparser.h \
    parser/partypokerparser.h \
    parser/fulltiltparser.h \
    converter.h \
    encodingdetector.h

RESOURCES += \
		resources.qrc

win32:CONFIG(release, debug|release): LIBS += -L$${BOOST}/stage/lib/ -lboost_regex-vc100-mt-1_52
else:win32:CONFIG(debug, debug|release): LIBS += -L$${BOOST}/stage/lib/ -lboost_regex-vc100-mt-gd-1_52

win32: LIBS += $${WINLIB}/AdvAPI32.lib


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../lib/ -libpplib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../lib/ -libpplibd

INCLUDEPATH += $$PWD/../ibpp
DEPENDPATH += $$PWD/../ibpp

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../lib/ibpplib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../lib/ibpplibd.lib

win32:CONFIG(release, debug|release): LIBS += -L$${CPPLIB}/poco/lib/ -lPocoFoundationmd
else:win32:CONFIG(debug, debug|release): LIBS += -L$${CPPLIB}/poco/lib/ -lPocoFoundationmdd

INCLUDEPATH += $${CPPLIB}/poco/Foundation/include
DEPENDPATH += $${CPPLIB}/poco/bin
