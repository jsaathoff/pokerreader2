#include "EncodingDetector.h"

#ifdef Q_WS_WIN
#include <QFile>
#endif

//
// code is based on wxAutoConv() code from wxWidgets
//
Encoding EncodingDetector::GetEncoding( const char *src, size_t srcLen )
{
    if ( srcLen < 2 )
    {
        // minimal BOM is 2 bytes so bail out immediately and simplify the code
        // below which wouldn't need to check for length for UTF-16 cases
            return Encoding::None;
    }

    // examine the buffer for BOM presence
    //
    // see http://www.unicode.org/faq/utf_bom.html#BOM
    switch ( *src++ )
    {
        case '\0':
            // could only be big endian UTF-32 (00 00 FE FF)
            if ( srcLen >= 4 &&
                    src[0] == '\0' &&
                        src[1] == '\xfe' &&
                            src[2] == '\xff' )
            {
                            return Encoding::Unicode32BE;
            }
            break;

        case '\xfe':
            // could only be big endian UTF-16 (FE FF)
            if ( *src++ == '\xff' )
            {
                return Encoding::Unicode16BE;
            }
            break;

        case '\xff':
            // could be either little endian UTF-16 or UTF-32, both start
            // with FF FE
            if ( *src++ == '\xfe' )
            {
                return srcLen >= 4 && src[0] == '\0' && src[1] == '\0'
                            ? Encoding::Unicode32LE
                            : Encoding::Unicode16LE;
            }
            break;

        case '\xef':
            // is this UTF-8 BOM (EF BB BF)?
            if ( srcLen >= 3 && src[0] == '\xbb' && src[1] == '\xbf' )
            {
                            return Encoding::UTF8;
            }
            break;
    }

    return Encoding::ANSI;
}

std::string EncodingDetector::readUTF8File(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}

std::string EncodingDetector::readUTF16BEFile(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}

std::string EncodingDetector::readUTF16LEFile(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}

std::string EncodingDetector::readUTF32BEFile(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}

std::string EncodingDetector::readUTF32LEFile(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}

std::string EncodingDetector::readANSIFile(std::string filename)
{
#ifdef Q_WS_WIN
    QFile file(filename.fromStdString());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

    QString content = file.readAll();

    file.close();

    return content.toUtf8();
#endif
    return "";
}
