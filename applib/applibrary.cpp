#include "applibrary.h"
#include <QChar>

AppLibrary::AppLibrary()
{

}

double AppLibrary::parseDouble(QString input)
{
	double returnvalue = 0;
	bool isDouble;
	int i = 0;
	int count = 0;
	QChar c = 0;
	QString tmpDouble,tmp1;
	bool gotSep = false;

	returnvalue = input.toDouble(&isDouble);

	if(isDouble) // is valid double
		return returnvalue; // return the value
	else // parse the string
	{
		for(i = 0; i < input.length(); i++)
		{
			c = input[i];

			if(c.isDigit())
			{
				tmp1.append(c);
			}
			else if( (c == '.') || (c == ','))
			{
				if(tmp1.length() > 3)
				{
					tmpDouble.append(".");
					tmpDouble.append(tmp1);
					tmp1.clear();
				}
				else if(tmp1.length() == 3)
				{
					tmpDouble.append(tmp1); // thousand sep, just append
					tmp1.clear();
				}
				else if(tmp1.length() == 2)
				{
					tmpDouble.append("."); // decimal sep, just append
					tmpDouble.append(tmp1);
					tmp1.clear();
				}
				else if(tmp1.length() == 1)
				{
					tmpDouble.append(tmp1); // just
					tmp1.clear();
				}
				else
				{
					tmpDouble.append(tmp1);
					tmp1.clear();
				}
			}
		}

		if(!tmp1.isEmpty())
		{
			tmpDouble.append("."); // all last found, append as dec. sep
			tmpDouble.append(tmp1);
			tmp1.clear();
		}

		returnvalue = tmpDouble.toDouble(&isDouble); // convert to double
		if(isDouble)
			return returnvalue; // return
		else
			return 0;
	}
	return 0;
}

QDateTime AppLibrary::parseDateTime(QString input)
{
	QDateTime dtOut;

	if(dtOut.isNull())
	{
		dtOut = QDateTime::fromString(input, "yyyy/M/d h:m:s");

		if(dtOut.isValid())
			return dtOut;
		else
		{
			dtOut = QDateTime::fromString(input, "d.M.yy h:m:s");

			if(dtOut.isValid())
				return dtOut;
		}
	}
}
