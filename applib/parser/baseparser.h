#ifndef BASEPARSER_H
#define BASEPARSER_H

#include <map>
#include <stdlib.h>
#include <stdio.h>
#include "../converter.h"
#include "../datatypes/currencies.h"
#include "../datatypes/handtypes.h"
#include "../datatypes/limittypes.h"
#include "../parser/parserstates.h"
#include "../parser/importcontainer.h"
#include <vector>
#include <boost/format.hpp>

#define MAXNUMBEROFMATCHES 25

class ImportContainer;
class EncodingDetector;

namespace pcrecpp {
 class RE;
}

struct Date
{
    int day;
    int month;
    int year;

    Date(int day, int month, int year)
    {
        this->day = day;
        this->month = month;
        this->year = year;
    }
};

struct Time
{
    int hour;
    int minute;
    int second;
    std::string tz;

    Time(int hour, int minute, int second, std::string tz)
    {
        this->hour = hour;
        this->minute = minute;
        this->second = second;
        this->tz = tz;
    }
};

struct DateTime
{
    int day;
    int month;
    int year;

    int hour;
    int minute;
    int second;

    std::string tz;

    DateTime(int day, int month, int year, int hour = 0, int minute = 0, int second = 0, std::string tz = "")
    {
        this->day = day;
        this->month = month;
        this->year = year;
        this->hour = hour;
        this->minute = minute;
        this->second = second;
        this->tz = tz;
    }

    void addTime(Time timein)
    {
        this->hour = timein.hour;
        this->minute = timein.minute;
        this->second = timein.second;
        this->tz = timein.tz;
    }

};


class BaseParser
{
public:
    BaseParser();
    ~BaseParser();

    void setContainer(ImportContainer * val);

    ImportContainer * getContainer();


private:
    ImportContainer * m_Container;

    EncodingDetector * EncDetector;

    char encodingbuffer[5];

protected:

    double getDouble(std::string val);

    void setState(int state);

    int getState();

    void IncErrorCount();

    int ErrorCount();

    void setContentToParse(std::string parse);

    void setFileName(std::string val);

    std::string getFileName();

    std::string getContentToParse();

    int getMonthfromString(std::string month);

    int getDayfromString(std::string day);

    std::map<std::string, int> KeyWordCurrency;

    double getdoubleret;

    bool ok;

    int m_errorcount;

    bool m_Validhand;

    int m_Handstate;

    bool m_gotCurrency;

    std::string m_parseContent;

    std::string m_fileName;

    std::string tempworkstring;

    int m_parserState;

};

#endif // BASEPARSER_H
