#ifndef PARSEHAND_H
#define PARSEHAND_H

#include "../datatypes/cashhand.h"

class PlayerHandAction;

class ParseHand : public CashHand
{
public:
    ParseHand();
    ~ParseHand();

    int getHandState();

    void setHandState(int val);

private:

    int m_handstate;
};

#endif // PARSEHAND_H
