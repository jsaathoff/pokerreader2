#include "baseparsertext.h"
#include "../datatypes/handstates.h"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string_regex.hpp>
#include "importcontainer.h"


BaseParserText::BaseParserText() : BaseParser()
{
    m_Validhand = false;
    m_Handstate = Handstates::PREFLOP;
    eatinput = false;

    CURR_SHORT = "\$";

    CURR_LONG = "EUR|USD";
}

BaseParserText::~BaseParserText()
{

}

bool BaseParserText::parseLimits(std::string input)
{
    if(parseLimit1(input)) // sb or limit1
    {
        if(parseLimit2(input)) // bb or limit2
        {
            return true;
        }
        else
            return false;
    }
    else
    {
        return false;
    }
}

bool BaseParserText::parseLimit1(std::string &input)
{
    /*
    if(std::isdigit(input[0]))
        getContainer()->CurrentHand()->setIdCurrency(PokerCurrency::PLAYMONEY);
    else
    {
        //getContainer()->CurrentHand()->setIdCurrency(KeyWordCurrency[input[0]]);
        input = std::string((input.begin()+1), input.end());
    }

    m_workString = std::string(input.begin(), boost::algorithm::find_first(input, "/").begin());
    getContainer()->CurrentHand()->setLimit1(getDouble(m_workString));

    input = std::string(boost::algorithm::find_first(input, "/").end(), input.end());
    */
    return true;
}

bool BaseParserText::parseLimit2(std::string &input)
{
    /*
    if(std::isdigit(input[0]))
        getContainer()->CurrentHand()->setIdCurrency(PokerCurrency::PLAYMONEY);
    else
    {
        //getContainer()->CurrentHand()->setIdCurrency(KeyWordCurrency[input[0]]);
        input = std::string((input.begin()+1), input.end());
    }

    m_workString = input;
    getContainer()->CurrentHand()->setLimit2(getDouble(m_workString));
    */
    return true;
}

bool BaseParserText::parseLimitHandType(std::string input)
{
    bool result = false;

    /*
    getContainer()->CurrentHand()->setIdLimittype(LimitTypes::NONE);
    getContainer()->CurrentHand()->setIdGametype(Handtype::NONE);

    // get limit type
    if(boost::algorithm::ifind_first(input, "no limit"))
    {
        getContainer()->CurrentHand()->setIdLimittype(LimitTypes::NOLIMIT);
        result = true;
    }
    else if(boost::algorithm::ifind_first(input, "pot limit"))
    {
        getContainer()->CurrentHand()->setIdLimittype(LimitTypes::POTLIMIT);
        result = true;
    }
    else if(boost::algorithm::ifind_first(input, "limit"))
    {
        getContainer()->CurrentHand()->setIdLimittype(LimitTypes::FIXEDLIMIT);
        result = true;
    }
    // get limit type

    // get type of game
    if(boost::algorithm::ifind_first(input, "hold'em"))
    {
        getContainer()->CurrentHand()->setIdGametype(Handtype::HOLDEM);
        result = true;
    }
    else if(boost::algorithm::ifind_first(input, "omaha"))
    {
        getContainer()->CurrentHand()->setIdGametype(Handtype::OMAHA);
        result = true;
    }
    // get type of game
    */
    return result;

}

bool BaseParserText::parseDateTime(std::string input, DateTime &dt)
{
    return false; // TODO: implement
}

void BaseParserText::setSplitString(std::string val)
{
    m_sSplitString = val;
}

std::string BaseParserText::getSplitString()
{
    return m_sSplitString;
}
