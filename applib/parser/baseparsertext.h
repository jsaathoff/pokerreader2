#ifndef BASEPARSERTEXT_H
#define BASEPARSERTEXT_H

#include <QObject>
#include "../parser/baseparser.h"
#include "parsehand.h"
#include "parserstates.h"


class BaseParserText : public BaseParser
{
public:
    BaseParserText();
    ~BaseParserText();

    void setSplitString(std::string val);

protected:

    virtual void initRegexes() = 0;

    std::string getSplitString();

    virtual bool parseContent(std::string m_parseContent) = 0;

    virtual bool parseHeader(std::string &input) = 0;

    bool parseLimit2(std::string &input);

    bool parseLimit1(std::string &input);

    bool parseLimits(std::string input);

    bool parseLimitHandType(std::string input);

    bool parseFile(std::string filename);

    bool parseDateTime(std::string input, DateTime &dt);

    int m_parserState;

    bool eatinput;

    std::map<std::string, int> RENAMECAPTURE;

    std::string m_workString;

    std::string CURR_SHORT;

    std::string CURR_LONG;

    std::string m_sSplitString;
};

#endif // BASEPARSERTEXT_H
