#ifndef HANDIMPORTER_H
#define HANDIMPORTER_H

#include <iostream>
#include <string>
#include "../AppLib_global.h"

class PokerStarsParser;
class FullTiltParser;

class HandImporter
{
public:
    HandImporter();
    ~HandImporter();

    void ParseContent();

    void setParseContent(std::string parsecontent);

    std::string getParseContent();

private:

    PokerStarsParser * pokerstarsparser;
    FullTiltParser * fulltiltparser;

    std::string m_parsecontent;

};

#endif // HANDIMPORTER_H
