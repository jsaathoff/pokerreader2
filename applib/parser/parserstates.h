#ifndef PARSERSTATES_H
#define PARSERSTATES_H

namespace ParserStates {
    enum ParserStates {
        EMPTY,
        GOT_HANDNAME,
        GOT_TOURNAMENTNAME,
        GOT_GAMELIMITTYPE,
        GOT_CURRENCY,
        GOT_HANDLIMIT,
        GOT_TOURNAMENTLIMIT,
        GOT_LIMIT1,
        GOT_LIMIT2,
        GOT_DATE,
        GOT_TIME,
        GOT_HANDHEADER,
        GOT_TABLENAME,
        GOT_BUTTONSEAT,
        GOT_NUMBEROFPLAYERS,
        GOT_TABLEINFO
    };
};
#endif // PARSERSTATES_H
