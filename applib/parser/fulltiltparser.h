#ifndef FULLTILTPARSER_H
#define FULLTILTPARSER_H

#include "baseparsertext.h"

#define MAXFTPARSERHEADERARGS 14 // TODO: max number of captions in header

namespace pcrecpp {
 class RE;
}

class FullTiltParser : public BaseParserText
{
public:
    FullTiltParser();
    ~FullTiltParser();

    bool parseContent(std::string m_parseContent);

private:
     bool parseHeader(std::string &input);

     bool parseTournamentInfo(std::string &input);

     bool parseTable(std::string &input);

     bool parseTableMaxSeats(std::string &input);

     void initRegexes();

     pcrecpp::RE * m_regexHeader;
};

#endif // FULLTILTPARSER_H
