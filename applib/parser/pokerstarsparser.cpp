#include "pokerstarsparser.h"
#include <QFile>
#include <QStringList>
#include "importcontainer.h"
#include <sstream>
#include <fstream>
#include <iostream>

#include <QDebug>

#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string_regex.hpp>

#include <string>

PokerStarsParser::PokerStarsParser() : BaseParserText()
{
    initRegexes();
}

PokerStarsParser::~PokerStarsParser()
{

}

void PokerStarsParser::initRegexes()
{

    /*m_regHeaderInfo = new ParseRegEx("\\A(?:(?:PokerStars)\\s+(?:Game|Hand))\\s+#(\\d+):\\s+([\\w\\s']+)\\s+\\(((?#CURRSYMBOL))([\\d.,]+)/((?#CURRSYMBOL))([\\d.,]+)\\)\\s+-\\s+(?:([\\d]{4})/([\\d]{2})/([\\d]{2}))\\s+(?:([\\d]{1,2})\\:([\\d]{1,2})\\:([\\d]{1,2})\\s+(\\w+))", regoption);
    m_regHeaderInfo->AddReplace("(?#CURRSYMBOL)", CURR_SHORT);
    m_regHeaderInfo->ReplacePattern();*/

    /*m_regTableInfo = new ParseRegEx("\\ATable\\s+'(.+)'\\s+(\\d{1,2})-max\\s+(?:seat)\\s+#(\\d{1,2})", regoption);

    m_regSeatInfo = new ParseRegEx("\\Aseat\\s+(\\d{1,2}):\\s(.+)\\s\\((?#CURRSYMBOL)([\\d.,]+)\\s", regoption);
    m_regSeatInfo->AddReplace("(?#CURRSYMBOL)", CURR_SHORT);
    m_regSeatInfo->ReplacePattern();
    */
    // HEADER

    boost::format fmt = boost::format("\\A(PokerStars|POKERSTARS)\\s(Game|Hand|Home\\s+Game|Home\\sGame\\sHand|Game|Zoom\\sHand|GAME)\\s+\\#([0-9]+):\\s+(?:(Tournament)\\s+\\#(\\d+),\\s+(\\$)([\\d,.]+)\\+(\\$)([\\d,.]+)(?:\\+(\\$)([\\d,.]+))*\\s+)*(Hold'em)\\s+(Limit|No\\s+Limit|Pot\\s+Limit)\\s+-\\s+(.+)$") % CURR_SHORT % CURR_LONG;

    m_sHeaderInfo = fmt.str();
    qDebug() << m_sHeaderInfo.c_str();
    m_regHeaderInfo = new QRegularExpression(QString::fromStdString(m_sHeaderInfo));
    // HEADER

}

bool PokerStarsParser::parseContent(std::string parsecontent)
{
    std::string target, handname, hand;
    //boost::iterator_range<std::string::iterator> doublephandsep = boost::algorithm::find_first(filecontent, L":");

    bool result = false;
    result = true;

    std::vector<std::string> hands;
    std::vector<std::string> lines;

    int linecount = 0;

    setContentToParse(parsecontent);

    // fetch hands
    boost::algorithm::split_regex(hands, getContentToParse(), boost::regex("pokerstars game|hand"));

    for(size_t i=0; i < hands.size(); i++)
    {
        hand = hands[i];
        linecount = 0;

        if(!hand.empty())
            boost::split(lines, hand, boost::algorithm::is_any_of("\n")); // split line by line

        if(lines.size() > 0)
        {
            if(!lines[linecount].empty())
            {
                lines[linecount] = getSplitString() + lines[linecount]; // build a complete string by adding the necc. splitter

                if(parseHeader(lines[linecount++])) // parse header info
                {
                    if(parseTableInfo(lines[linecount++])) // parse table info
                    {
                        for(int i = 0;  i < 10; i++)
                        {
                            std::string pud = lines[linecount].substr(0,4);
                            if(lines[linecount].substr(0,4) == "Seat")
                            {
                                parseSeatInfo(lines[linecount++]);
                            }
                            else
                                break;
                        }
                    }
                }
                //else
                //{
                    return false;
                //}
            }
        }
    }

    return true;
}

bool PokerStarsParser::parseHeader(std::string &input)
{
    bool result = false;

    if(m_regHeaderInfo->match(QString::fromStdString(input)).hasMatch())
    {
        result = true;
    }

    return result;
}

bool PokerStarsParser::parseTableInfo(std::string &input)
{
    std::string tablename;
    int maxseats = 0;
    int button = 0;

    /*
    if(m_regTableInfo->PartialMatch(input, &tablename, &maxseats, &button))
    {
        getContainer()->CurrentHand()->setTablename(tablename);
        getContainer()->CurrentHand()->setMaxplayer(maxseats);
        getContainer()->CurrentHand()->setDealerseat(button);

        setState(ParserStates::GOT_TABLEINFO);

        return true;
    }
    */

    return false;
}

bool PokerStarsParser::parseSeatInfo(std::string &input)
{
    /*
    std::string playername;
    int seatno = 0;
    double startamount = 0;

    //std::string pattern = m_regSeatInfo->getPattern();
    if(m_regSeatInfo->PartialMatch(input, &playername, &seatno, &startamount))
    {
        return true;
    }
    */
    return false;
}
