#include "importcontainer.h"
#include "parsehand.h"

ImportContainer::ImportContainer(QObject *parent) :
    QObject(parent)
{

}

void ImportContainer::NewHand()
{
    m_Hand = new ParseHand();
}

ParseHand * ImportContainer::CurrentHand()
{
    return m_Hand;
}
