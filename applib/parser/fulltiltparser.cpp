
#include "fulltiltparser.h"
#include "importcontainer.h"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string_regex.hpp>

#ifdef QT_DEBUG
#include <QDebug>
//#include <QMessageBox>
#endif

FullTiltParser::FullTiltParser() : BaseParserText()
{
    RENAMECAPTURE["HANDNAME"] = 1;
    RENAMECAPTURE["TOURNAMENTDESC"] = 2;
}

FullTiltParser::~FullTiltParser()
{

}

void FullTiltParser::initRegexes()
{

}

bool FullTiltParser::parseContent(std::string parsecontent)
{
    std::string target, handname, hand;
    //boost::iterator_range<std::string::iterator> doublephandsep = boost::algorithm::find_first(filecontent, L":");

    bool result = false;
    result = true;

    std::vector<std::string> hands;
    std::vector<std::string> lines;

    setContentToParse(parsecontent);

    // fetch hands
    boost::algorithm::split_regex(hands, getContentToParse(), boost::regex("Full Tilt Poker Game "));

    for(size_t i=0; i < hands.size(); i++)
    {
        hand = hands[i];

        if(!hand.empty())
            boost::split(lines, hand, boost::algorithm::is_any_of("\n"));

        if(lines.size() > 0)
        {
            if(!lines[0].empty())
            {
                if(parseHeader(lines[0]))
                {

                }
                //else
                //{
                    return false;
                //}
            }
        }
    }

    return true;
}

bool FullTiltParser::parseHeader(std::string &input)
{

    std::string handname, toparse;
    if(boost::algorithm::starts_with(input, "#")) // a valid hand starts with #
    {
        boost::algorithm::trim_left(input); // trim
        handname = std::string( (input.begin()+1), boost::algorithm::find_first(input, ":").begin()); // get all till :
        input = std::string(boost::algorithm::find_first(input, ":").end(), input.end()); // replace it, eat up

        if(!handname.empty()) // handname there
        {
            getContainer()->NewHand(); // create a new hand
            getContainer()->CurrentHand()->setName(handname); // self-explaining, set the name
        }
        else
            return false;
    }

    bool tn = parseTournamentInfo(input); // get tournament information

    if(parseTable(input)) // parse the table info
    {
        if(parseTableMaxSeats(input)) // get max seats if there
        {
            toparse = std::string(input.begin(), boost::algorithm::find_first(input, " - ").begin());
            input = std::string(boost::algorithm::find_first(input, " - ").end(), input.end());

            if(parseLimits(toparse)) // parse limits, limit1 and limit2 sb/bb, get till first occurence of " -" which is a delimiter
            {
                toparse = std::string(input.begin(), boost::algorithm::find_first(input, " - ").begin());
                input = std::string(boost::algorithm::find_first(input, " - ").end(), input.end());

                if(parseLimitHandType(toparse)) // get limit and the handtype (no limit, limit, pot limit) and (hold'em, omaha)
                {
                    toparse = std::string(input.begin(), boost::algorithm::find_last(input, " - ").begin());
                    return true;
                }
                else
                    return false;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool FullTiltParser::parseTable(std::string &input)
{
    std::string tablename;
    bool result = false;
    boost::algorithm::trim_left(input);

    if(boost::algorithm::starts_with(input, "Table "))
    {
        boost::algorithm::replace_first(input, "Table ", "");
        tablename = std::string(input.begin(), boost::algorithm::find_first(input, " -").begin());
        input = std::string(boost::algorithm::find_first(input, " -").end(), input.end());
        boost::algorithm::trim_left(input);
        //getContainer()->CurrentHand()->(QString::fromStdString(handname));

        result = parseTableMaxSeats(tablename);
        //getContainer()->CurrentHand()->setTablename(tablename);
        return result;
    }
    else
        return false;
}

bool FullTiltParser::parseTableMaxSeats(std::string &input)
{
    bool foundmaxplayer = false;

    // find and set max player, replace input with match if matched
    if(boost::algorithm::ends_with(input, "(10 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(10);
        boost::algorithm::replace_first(input, "(10 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(9 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(9);
        boost::algorithm::replace_first(input, "(9 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(8 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(8);
        boost::algorithm::replace_first(input, "(8 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(7 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(7);
        boost::algorithm::replace_first(input, "(7 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(6 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(6);
        boost::algorithm::replace_first(input, "(6 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(5 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(5);
        boost::algorithm::replace_first(input, "(5 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(4 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(4);
        boost::algorithm::replace_first(input, "(4 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(3 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(3);
        boost::algorithm::replace_first(input, "(3 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else if(boost::algorithm::ends_with(input, "(2 max)"))
    {
        getContainer()->CurrentHand()->setMaxplayer(2);
        boost::algorithm::replace_first(input, "(2 max)", "");
        foundmaxplayer = true;
        return true;
    }
    else
    {
        if(!input.empty())
        {
            getContainer()->CurrentHand()->setMaxplayer(9);
            return true;
        }
    }
    return false;
}

bool FullTiltParser::parseTournamentInfo(std::string &input)
{
    std::string tournamentinfo;
    std::string tournamentname;

    if(boost::algorithm::find_first(input, ",")) // if the token , was found at beginning...must be a tournament
    {
        tournamentinfo = std::string(input.begin(), boost::algorithm::find_first(input, ",").end()); // fetch all till , occurs
        boost::algorithm::trim_left(tournamentinfo); // trim
        boost::algorithm::trim_left(input);

        if(!tournamentinfo.empty()) // tournamentinfo exists
        {
            // parse the tournament name
            tournamentname = std::string(boost::algorithm::find_last(tournamentinfo, "(").end(), boost::algorithm::find_last(tournamentinfo, ")").begin());
            //getContainer()->CurrentHand()->setTournamentname(tournamentname);
            boost::algorithm::replace_first(input, tournamentinfo, ""); // replace the tournament info to eat up
            return true;
        }
        else
            return false;
    }
    else
        return false;
}


