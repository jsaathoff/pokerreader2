#ifndef PARTYPOKERPARSER_H
#define PARTYPOKERPARSER_H

#include "baseparsertext.h"

class PartyPokerParser : public BaseParserText
{
public:
    PartyPokerParser();
    ~PartyPokerParser();

    bool parseFile(std::wstring filename);

    bool parseContent();
};

#endif // PARTYPOKERPARSER_H
