#include "baseparser.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <QFile>

#include "importcontainer.h"
#include <boost/algorithm/string.hpp>
#include <QDebug>
#include "../encodingdetector.h"

BaseParser::BaseParser()
{
    ok = false;
    getdoubleret = 0.0;
    m_errorcount = 0;
    m_gotCurrency = false;

    encodingbuffer[0] = 0;
    encodingbuffer[1] = 0;
    encodingbuffer[2] = 0;
    encodingbuffer[3] = 0;
    encodingbuffer[4] = 0;


    EncDetector = new EncodingDetector();

    m_parserState = -1;

    KeyWordCurrency["$"] = PokerCurrency::USD;
    KeyWordCurrency["€"] = PokerCurrency::EURO;

}

BaseParser::~BaseParser()
{

}

double BaseParser::getDouble(std::string val)
{
    try
    {
        getdoubleret = boost::lexical_cast<double>(val);
    }
    catch(boost::bad_lexical_cast &)
    {
        getdoubleret = 0;
        return getdoubleret;
    }

    return getdoubleret;
}

void BaseParser::IncErrorCount()
{
    m_errorcount++;
}

int BaseParser::ErrorCount()
{
    return m_errorcount;
}

void BaseParser::setContainer(ImportContainer * val)
{
    m_Container = val;
}

ImportContainer * BaseParser::getContainer()
{
    return m_Container;
}

int BaseParser::getMonthfromString(std::string month)
{
    boost::algorithm::to_lower(month);

    if(month == "january")
        return 1;
    if(month == "februar")
        return 2;
    if(month == "march")
        return 3;
    if(month == "april")
        return 4;
    if(month == "may")
        return 5;
    if(month == "june")
        return 6;
    if(month == "july")
        return 7;
    if(month == "august")
        return 8;
    if(month == "september")
        return 9;
    if(month == "october")
        return 10;
    if(month == "november")
        return 11;
    if(month == "december")
        return 12;

    return 0;

}

int BaseParser::getDayfromString(std::string day)
{
    return 1;
}

void BaseParser::setContentToParse(std::string parse)
{
    m_parseContent = parse;
}

std::string BaseParser::getContentToParse()
{
    return m_parseContent;
}

void BaseParser::setFileName(std::string val)
{
    m_fileName = val;
}

std::string BaseParser::getFileName()
{
    return m_fileName;
}

void BaseParser::setState(int state)
{
    m_parserState = state;
}

int BaseParser::getState()
{
    return m_parserState;
}
