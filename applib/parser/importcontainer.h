#ifndef IMPORTCONTAINER_H
#define IMPORTCONTAINER_H

#include <QObject>
#include <QVector>

class ParseHand;

class ImportContainer : public QObject
{
    Q_OBJECT
public:
    explicit ImportContainer(QObject *parent = 0);

    void NewHand();

    ParseHand * CurrentHand();

private:
    QVector<ParseHand*> m_Hands;

    ParseHand * m_Hand;

signals:

public slots:

};

#endif // IMPORTCONTAINER_H
