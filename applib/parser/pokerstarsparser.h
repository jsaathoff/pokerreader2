#ifndef POKERSTARSPARSER_H
#define POKERSTARSPARSER_H

#include "baseparsertext.h"

class QRegularExpression;

class ParseRegEx;

class PokerStarsParser : public BaseParserText
{
public:

    PokerStarsParser();
    ~PokerStarsParser();

    bool parseContent(std::string m_parseContent);

private:

    bool parseHeader(std::string &input);

    bool parseTableInfo(std::string &input);

    bool parseSeatInfo(std::string &input);

    void initRegexes();

    QRegularExpression * m_regHeaderInfo;

    QRegularExpression * m_regTableInfo;

    QRegularExpression * m_regSeatInfo;

    std::string m_sHeaderInfo;

    std::string m_sTableInfo;

    std::string m_sSeatInfo;

    /*
    void Parse(QStringList * list);

    void ParseHeader(QString val);

    void ParseLimitType(QString val);

    void ParseCurrency(QString val);

    void ParseLimits(QString val);

    void ParseLimit1(QString val);

    void ParseLimit2(QString val);

    void ParseDateTime(QString val);
    */
};

#endif // POKERSTARSPARSER_H
