#include "handimporter.h"

#include "pokerstarsparser.h"
#include "fulltiltparser.h"

#include "importcontainer.h"


HandImporter::HandImporter()
{
    ImportContainer container;
    pokerstarsparser = new PokerStarsParser();
    fulltiltparser = new FullTiltParser();

    pokerstarsparser->setContainer(&container);
    fulltiltparser->setContainer(&container);
}

HandImporter::~HandImporter()
{
    delete pokerstarsparser;
    delete fulltiltparser;
}

void HandImporter::ParseContent()
{
    if(getParseContent().find("PokerStars ") != std::string::npos)
    {
        pokerstarsparser->parseContent(getParseContent());
    }
    else if(getParseContent().find("Full Tilt Poker ") != std::string::npos)
    {
        fulltiltparser->parseContent(getParseContent());
    }
}

void HandImporter::setParseContent(std::string parsecontent)
{
    m_parsecontent = parsecontent;
}

std::string HandImporter::getParseContent()
{
    return m_parsecontent;
}
