#include "applicationpaths.h"
//#include <QAbstractFileEngine>
#include <QDir>
#include <QFile>
#include <QTextStream>

ApplicationPaths::ApplicationPaths(QObject *parent) : QObject(parent)
{
    userdatadir = QDir::homePath() + "/PokerReader/";
    configDir = userdatadir + "config/";
    userdatabaseDir = userdatadir + "database/";
    userdatabaseFile = userdatabaseDir + "pokerreader_he.prdb";
}

QString ApplicationPaths::UserDatabaseDir()
{
    return userdatabaseDir;
}

QString ApplicationPaths::UserConfigDir()
{
    return configDir;
}

QString ApplicationPaths::UserDatabaseFile()
{
    return userdatabaseFile;
}

QString ApplicationPaths::UserDataDir()
{
    return userdatadir;
}

QString ApplicationPaths::ApplicationPath()
{
    return applicationDirPath;
}

void ApplicationPaths::setApplicationPath(QString path)
{
    applicationDirPath = path;
}
