#include "settings.h"

Settings *Settings::Instance()
{
	static Settings inst;
	return &inst;
}

Settings::Settings(QObject *parent) : QSettings(QSettings::IniFormat, QSettings::UserScope, VENDORNAME, APPNAME, parent)
{

}
