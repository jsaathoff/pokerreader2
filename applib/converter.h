#ifndef CONVERTER_H
#define CONVERTER_H

#include <ios>
#include <iostream>
#include <string>

class Converter
{
public:
    Converter();

    static std::string ToStdString(std::wstring input);
};

#endif // CONVERTER_H
