#ifndef APPLICATIONPATHS_H
#define APPLICATIONPATHS_H

#include <QObject>
#include "AppLib_global.h"

class APPLIBSHARED_EXPORT ApplicationPaths : public QObject
{
	Q_OBJECT
public:

	explicit ApplicationPaths(QObject *parent = 0);

	//! returns the path to the users database directory
	QString UserDatabaseDir();

	//! returns the path to configuration files
	QString UserConfigDir();

	//! returns the path to the users database file
	QString UserDatabaseFile();

	//! returns the path to the users datadir
	QString UserDataDir();
	
	//! return the path to the application install directory
	QString ApplicationPath();
	
	//! set the path to the application install directory
	void setApplicationPath(QString path);

signals:

public slots:

private:
	QString userdatadir;
	QString configDir;
	QString userdatabaseDir;
	QString userdatabaseFile;
	QString applicationDirPath;

};

#endif // APPLICATIONPATHS_H
