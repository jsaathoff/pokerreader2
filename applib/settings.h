#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include "AppLib_global.h"
#include "definitions.h"


class APPLIBSHARED_EXPORT Settings : public QSettings
{
	Q_OBJECT
public:

	static Settings  *Instance();

private:

	explicit Settings(QObject *parent = 0);

	Settings(const Settings&);

	Settings& operator= (const Settings&);

signals:

public slots:

};

#endif // SETTINGS_H
