#ifndef HANDTYPES_H
#define HANDTYPES_H

namespace Handtype
{
    enum Handtype
    {
        NONE,
        HOLDEM,
        OMAHA
    };
}
#endif // HANDTYPES_H
