#include "Hand.h"

Hand::Hand()
{
		plcount_turn = 0;
	plcount_river = 0;
	playercount = 0;
	plcount_showdown = 0;
	maxplayer = 0;
	buttonseat = 0;
	plcount_preflop = 0;
	plcount_flop = 0;
	limittype_id = 0;
	provider_id = 0;
	gametype_id = 0;
	currency_id = 0;
	limit_id = 0;
	importdate = IBPP::Time();
	handdate = IBPP::Time();
	importtime = IBPP::Date();
	handtime = IBPP::Date();
	boardcard1 = "";
	boardcard2 = "";
	boardcard3 = "";
	boardcard4 = "";
	boardcard5 = "";
	id = 0;
	agr_flop = 0;
	agr_turn = 0;
	agr_river = 0;
	agr_preflop = 0;
	limit1 = 0.0;
	limit2 = 0.0;
	pot_total = 0.0;
	pot_preflop = 0.0;
	pot_flop = 0.0;
	pot_turn = 0.0;
	pot_river = 0.0;
	handname = "";

}

Hand::~Hand()
{
}


void Hand::setPlcountTurn(unsigned int val)
{
	plcount_turn = val;
}

void Hand::setPlcountRiver(unsigned int val)
{
	plcount_river = val;
}

void Hand::setPlayercount(unsigned int val)
{
	playercount = val;
}

void Hand::setPlcountShowdown(unsigned int val)
{
	plcount_showdown = val;
}

void Hand::setMaxplayer(unsigned int val)
{
	maxplayer = val;
}

void Hand::setButtonseat(unsigned int val)
{
	buttonseat = val;
}

void Hand::setPlcountPreflop(unsigned int val)
{
	plcount_preflop = val;
}

void Hand::setPlcountFlop(unsigned int val)
{
	plcount_flop = val;
}

void Hand::setLimittypeId(unsigned long val)
{
	limittype_id = val;
}

void Hand::setProviderId(unsigned long val)
{
	provider_id = val;
}

void Hand::setGametypeId(unsigned long val)
{
	gametype_id = val;
}

void Hand::setCurrencyId(unsigned long val)
{
	currency_id = val;
}

void Hand::setLimitId(unsigned long val)
{
	limit_id = val;
}

void Hand::setImportdate(IBPP::Date val)
{
	importdate = val;
}

void Hand::setHanddate(IBPP::Date val)
{
	handdate = val;
}

void Hand::setImporttime(IBPP::Time val)
{
	importtime = val;
}

void Hand::setHandtime(IBPP::Time val)
{
	handtime = val;
}

void Hand::setBoardcard1(std::string val)
{
	boardcard1 = val;
}

void Hand::setBoardcard2(std::string val)
{
	boardcard2 = val;
}

void Hand::setBoardcard3(std::string val)
{
	boardcard3 = val;
}

void Hand::setBoardcard4(std::string val)
{
	boardcard4 = val;
}

void Hand::setBoardcard5(std::string val)
{
	boardcard5 = val;
}

void Hand::setId(__int64 val)
{
	id = val;
}

void Hand::setAgrFlop(__int64 val)
{
	agr_flop = val;
}

void Hand::setAgrTurn(__int64 val)
{
	agr_turn = val;
}

void Hand::setAgrRiver(__int64 val)
{
	agr_river = val;
}

void Hand::setAgrPreflop(__int64 val)
{
	agr_preflop = val;
}

void Hand::setLimit1(double val)
{
	limit1 = val;
}

void Hand::setLimit2(double val)
{
	limit2 = val;
}

void Hand::setPotTotal(double val)
{
	pot_total = val;
}

void Hand::setPotPreflop(double val)
{
	pot_preflop = val;
}

void Hand::setPotFlop(double val)
{
	pot_flop = val;
}

void Hand::setPotTurn(double val)
{
	pot_turn = val;
}

void Hand::setPotRiver(double val)
{
	pot_river = val;
}

void Hand::setHandname(std::string val)
{
	handname = val;
}



unsigned int Hand::getPlcountTurn()
{
	return plcount_turn;
}

unsigned int Hand::getPlcountRiver()
{
	return plcount_river;
}

unsigned int Hand::getPlayercount()
{
	return playercount;
}

unsigned int Hand::getPlcountShowdown()
{
	return plcount_showdown;
}

unsigned int Hand::getMaxplayer()
{
	return maxplayer;
}

unsigned int Hand::getButtonseat()
{
	return buttonseat;
}

unsigned int Hand::getPlcountPreflop()
{
	return plcount_preflop;
}

unsigned int Hand::getPlcountFlop()
{
	return plcount_flop;
}

unsigned long Hand::getLimittypeId()
{
	return limittype_id;
}

unsigned long Hand::getProviderId()
{
	return provider_id;
}

unsigned long Hand::getGametypeId()
{
	return gametype_id;
}

unsigned long Hand::getCurrencyId()
{
	return currency_id;
}

unsigned long Hand::getLimitId()
{
	return limit_id;
}

IBPP::Date Hand::getImportdate()
{
	return importdate;
}

IBPP::Date Hand::getHanddate()
{
	return handdate;
}

IBPP::Time Hand::getImporttime()
{
	return importtime;
}

IBPP::Time Hand::getHandtime()
{
	return handtime;
}

std::string Hand::getBoardcard1()
{
	return boardcard1;
}

std::string Hand::getBoardcard2()
{
	return boardcard2;
}

std::string Hand::getBoardcard3()
{
	return boardcard3;
}

std::string Hand::getBoardcard4()
{
	return boardcard4;
}

std::string Hand::getBoardcard5()
{
	return boardcard5;
}

__int64 Hand::getId()
{
	return id;
}

__int64 Hand::getAgrFlop()
{
	return agr_flop;
}

__int64 Hand::getAgrTurn()
{
	return agr_turn;
}

__int64 Hand::getAgrRiver()
{
	return agr_river;
}

__int64 Hand::getAgrPreflop()
{
	return agr_preflop;
}

double Hand::getLimit1()
{
	return limit1;
}

double Hand::getLimit2()
{
	return limit2;
}

double Hand::getPotTotal()
{
	return pot_total;
}

double Hand::getPotPreflop()
{
	return pot_preflop;
}

double Hand::getPotFlop()
{
	return pot_flop;
}

double Hand::getPotTurn()
{
	return pot_turn;
}

double Hand::getPotRiver()
{
	return pot_river;
}

std::string Hand::getHandname()
{
	return handname;
}


