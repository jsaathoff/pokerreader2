#ifndef HAND_H
#define HAND_H

#include <ibpp.h>

class Hand
{
public:
	Hand();
  
	~Hand();
  

	void setPlcountTurn(unsigned int val);
	void setPlcountRiver(unsigned int val);
	void setPlayercount(unsigned int val);
	void setPlcountShowdown(unsigned int val);
	void setMaxplayer(unsigned int val);
	void setButtonseat(unsigned int val);
	void setPlcountPreflop(unsigned int val);
	void setPlcountFlop(unsigned int val);
	void setLimittypeId(unsigned long val);
	void setProviderId(unsigned long val);
	void setGametypeId(unsigned long val);
	void setCurrencyId(unsigned long val);
	void setLimitId(unsigned long val);
	void setImportdate(IBPP::Date val);
	void setHanddate(IBPP::Date val);
	void setImporttime(IBPP::Time val);
	void setHandtime(IBPP::Time val);
	void setBoardcard1(std::string val);
	void setBoardcard2(std::string val);
	void setBoardcard3(std::string val);
	void setBoardcard4(std::string val);
	void setBoardcard5(std::string val);
	void setId(__int64 val);
	void setAgrFlop(__int64 val);
	void setAgrTurn(__int64 val);
	void setAgrRiver(__int64 val);
	void setAgrPreflop(__int64 val);
	void setLimit1(double val);
	void setLimit2(double val);
	void setPotTotal(double val);
	void setPotPreflop(double val);
	void setPotFlop(double val);
	void setPotTurn(double val);
	void setPotRiver(double val);
	void setHandname(std::string val);
  

	unsigned int getPlcountTurn();
	unsigned int getPlcountRiver();
	unsigned int getPlayercount();
	unsigned int getPlcountShowdown();
	unsigned int getMaxplayer();
	unsigned int getButtonseat();
	unsigned int getPlcountPreflop();
	unsigned int getPlcountFlop();
	unsigned long getLimittypeId();
	unsigned long getProviderId();
	unsigned long getGametypeId();
	unsigned long getCurrencyId();
	unsigned long getLimitId();
	IBPP::Date getImportdate();
	IBPP::Date getHanddate();
	IBPP::Time getImporttime();
	IBPP::Time getHandtime();
	std::string getBoardcard1();
	std::string getBoardcard2();
	std::string getBoardcard3();
	std::string getBoardcard4();
	std::string getBoardcard5();
	__int64 getId();
	__int64 getAgrFlop();
	__int64 getAgrTurn();
	__int64 getAgrRiver();
	__int64 getAgrPreflop();
	double getLimit1();
	double getLimit2();
	double getPotTotal();
	double getPotPreflop();
	double getPotFlop();
	double getPotTurn();
	double getPotRiver();
	std::string getHandname();
    
private:

	unsigned int plcount_turn;
	unsigned int plcount_river;
	unsigned int playercount;
	unsigned int plcount_showdown;
	unsigned int maxplayer;
	unsigned int buttonseat;
	unsigned int plcount_preflop;
	unsigned int plcount_flop;
	unsigned long limittype_id;
	unsigned long provider_id;
	unsigned long gametype_id;
	unsigned long currency_id;
	unsigned long limit_id;
	IBPP::Date importdate;
	IBPP::Date handdate;
	IBPP::Time importtime;
	IBPP::Time handtime;
	std::string boardcard1;
	std::string boardcard2;
	std::string boardcard3;
	std::string boardcard4;
	std::string boardcard5;
	__int64 id;
	__int64 agr_flop;
	__int64 agr_turn;
	__int64 agr_river;
	__int64 agr_preflop;
	double limit1;
	double limit2;
	double pot_total;
	double pot_preflop;
	double pot_flop;
	double pot_turn;
	double pot_river;
	std::string handname;
    
};

#endif // HAND_H
