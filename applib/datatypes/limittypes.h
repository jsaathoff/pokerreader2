#ifndef LIMITTYPES_H
#define LIMITTYPES_H

namespace LimitTypes
{
    enum LimitTypes
    {
        NONE,
        NOLIMIT,
        FIXEDLIMIT,
        POTLIMIT
    };
}
#endif // LIMITTYPES_H
