#ifndef ACTIONS_H
#define ACTIONS_H

namespace Action
{
    enum Action
    {
        NONE,
        PLAYERINIT,
        FOLD,
        CHECK,
        CALL,
        BET,
        SMALLBLIND,
        BIGBLIND,
        RAISE,
        ANTE,
        SMALLANDBIGBLIND,
        DEADSMALLBLIND,
        DEADBIGBLIND,
        SHOW,
        MUCK,
        DOESNOTSHOW,
        HOLECARD
    };
}
#endif // ACTIONS_H
