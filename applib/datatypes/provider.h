#ifndef PROVIDER_H
#define PROVIDER_H

namespace Provider
{
    enum Provider
    {
        NONE = 0,
        POKERSTARS = 100,
        FULLTILT = 200,
        PARTYPOKER = 300
    };
}
#endif // PROVIDER_H
