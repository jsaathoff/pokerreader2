#ifndef PARSEHAND_H
#define PARSEHAND_H
#include "hand.h"

class ParseHand : public Hand
{
public:
    ParseHand();

    void setTablename(std::string value);

    std::string getTablename();

private:
    std::string m_tableName;
};

#endif // PARSEHAND_H
