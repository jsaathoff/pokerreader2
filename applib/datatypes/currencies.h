#ifndef CURRENCIES_H
#define CURRENCIES_H

namespace PokerCurrency
{
    enum PokerCurrency
    {
        PLAYMONEY,
        USD,
        EURO,
        BRITISHPOUNDS
    };
}

#endif // CURRENCIES_H
