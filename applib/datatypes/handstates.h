#ifndef HANDSTATES_H
#define HANDSTATES_H

namespace Handstates
{
    enum Handstates
    {
        PREFLOP,
        FLOP,
        TURN,
        RIVER,
        SHOWDOWN
    };
}

#endif // HANDSTATES_H
