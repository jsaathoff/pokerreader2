#ifndef APPLIBRARY_H
#define APPLIBRARY_H

#include "AppLib_global.h"
#include <QString>
#include <QDateTime>

/*
///////////////////////////////////////////////////////////////////////////////
//  These policies can be used to parse thousand separated
//  numbers with at most 2 decimal digits after the decimal
//  point. e.g. 123,456,789.01
///////////////////////////////////////////////////////////////////////////////
template <typename T>
struct ts_real_policies : boost::spirit::qi::ureal_policies<T>
{
	//  2 decimal places Max
	template <typename Iterator, typename Attribute>
	static bool parse_frac_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		return boost::spirit::qi::
			extract_uint<T, 10, 1, 2, true>::call(first, last, attr);
	}

	//  No exponent
	template <typename Iterator>
	static bool parse_exp(Iterator&, Iterator const&)
	{
		return false;
	}

	//  No exponent
	template <typename Iterator, typename Attribute>
	static bool parse_exp_n(Iterator&, Iterator const&, Attribute&)
	{
		return false;
	}

	//  Thousands separated numbers
	template <typename Iterator, typename Attribute>
	static bool parse_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		using boost::spirit::qi::uint_parser;
		namespace qi = boost::spirit::qi;

		uint_parser<unsigned, 10, 1, 3> uint3;
		uint_parser<unsigned, 10, 3, 3> uint3_3;
		uint_parser<unsigned, 10, 2, 2> uint2_2;

		T result = 0;
		if (parse(first, last, uint3, result))
		{
			bool hit = false;
			T n;
			Iterator save = first;
			while (qi::parse(first, last, ',') && qi::parse(first, last, uint3_3, n))
			{
				result = result * 1000 + n;
				save = first;
				hit = true;
			}

			if( (first != last) && (qi::parse(first, last, &qi::matches['0' >> qi::int_], n)))
			{
				if( (first != last) && (qi::parse(first, last, uint2_2, n)))
				{
					result = result + (n * 0.01);
				}

				save = first;
				hit = true;
			}
			else if( (first != last) && (qi::parse(first, last, uint2_2, n)))
			{
				if( n > 9)
					result = result + (n * 0.01);
				else
					result = result + (n * 0.1);
				save = first;
				hit = true;
			}

			first = save;
			if (hit)
				attr = result;
			return hit;
		}
		return false;
	}
};
*/

class APPLIBSHARED_EXPORT AppLibrary {
public:
	AppLibrary();

	//! parse a double
	static double parseDouble(QString input);

	//! parse a datetime
	static QDateTime parseDateTime(QString input);
};

#endif // APPLIBRARY_H
