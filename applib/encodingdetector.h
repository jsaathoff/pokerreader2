#ifndef __ENCODINGDETECTOR__
#define __ENCODINGDETECTOR__

#include <ios>
#include <string>

enum Encoding { None, ANSI, UTF8, Unicode16LE, Unicode16BE, Unicode32LE, Unicode32BE };

class EncodingDetector
{
public:

    EncodingDetector() {};
    ~EncodingDetector() {};

    std::string readUTF8File(std::string filename);

    std::string readUTF16BEFile(std::string filename);

    std::string readUTF16LEFile(std::string filename);

    std::string readUTF32BEFile(std::string filename);

    std::string readUTF32LEFile(std::string filename);

    std::string readANSIFile(std::string filename);

    Encoding GetEncoding( const char *src, size_t srcLen );
};

#endif // __ENCODINGDETECTOR__
