#ifndef CONTENTWIDGET_H
#define CONTENTWIDGET_H

#include <QtWidgets/QWidget>

class ContentWidget : public QWidget
{
    Q_OBJECT
public:

    explicit ContentWidget(QWidget *parent = 0);
    //~ContentWidget();

private:

   void setupUi();

protected:

   void paintEvent(QPaintEvent* event);

signals:

public slots:

};

#endif // CONTENTWIDGET_H
