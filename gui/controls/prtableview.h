#ifndef PRTABLEVIEW_H
#define PRTABLEVIEW_H

#include <QtWidgets/QTableView>


class PRTableView : public QTableView
{
    Q_OBJECT
public:

    explicit PRTableView(QWidget * parent = 0 );
    //~PRTableView();

protected:

   void paintEvent(QPaintEvent* event);
};

#endif // PRTABLEVIEW_H
