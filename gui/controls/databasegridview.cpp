#include "databasegridview.h"
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>
#include <QtSql/QSqlDatabase>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include "imagebutton.h"
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QApplication>
#include "../dialogs/fullscreengridview.h"
#include "prtableview.h"

DatabaseGridView::DatabaseGridView(QWidget *parent) :
    QWidget(parent)
{
    //db = QSqlDatabase::addDatabase("QIBASE");
    // TODO: add connection details
    setupUi();
    retranslateUi();
}

/*DatabaseGridView::~DatabaseGridView()
{
    delete tableView;
    delete btnConfig;
    delete btnShowfullscreen;
    delete spacerbtn;
    delete mainLayout;
    delete tableLayout;
    delete btnLayout;
}*/

void DatabaseGridView::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

void DatabaseGridView::connectSignals()
{
    connect(btnShowfullscreen, SIGNAL(clicked()), this, SLOT(showFull()));
}

void DatabaseGridView::showFull()
{
    fullGrid->setGridView(this);
    fullGrid->show();
}

void DatabaseGridView::setupUi()
{
    mainLayout = new QHBoxLayout(this);
    tableLayout = new QHBoxLayout(this);
    btnLayout = new QVBoxLayout(this);
    tableView = new PRTableView(this);

    btnConfig = new ImageButton(this);
    btnShowfullscreen = new ImageButton(this);
    spacerbtn = new QSpacerItem(-1, -1, QSizePolicy::Minimum, QSizePolicy::Expanding);
    fullGrid = new FullScreenDatabaseGridView();
    fullGrid->hide();

    btnConfig->setObjectName("configGrid");
    btnShowfullscreen->setObjectName("showFullscreen");

    btnLayout->setContentsMargins(0,0,0,0);
    tableLayout->setContentsMargins(2,0,0,0);
    mainLayout->setContentsMargins(0,0,0,0);

    btnLayout->setSpacing(0);
    tableLayout->setSpacing(0);
    mainLayout->setSpacing(0);

    tableView->setContentsMargins(0,0,0,0);

    btnConfig->setContentsMargins(0, 0, 0, 0);
    btnShowfullscreen->setContentsMargins(0, 0, 0, 0);

    btnConfig->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    btnShowfullscreen->setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    tableLayout->addWidget(tableView, 1);
    mainLayout->addLayout(tableLayout, 1);
    mainLayout->addLayout(btnLayout, 0);

    btnLayout->addWidget(btnConfig);
    btnLayout->addWidget(btnShowfullscreen);
    btnLayout->addSpacerItem(spacerbtn);

    hideConfigButtons();
    showConfigButtons();
    connectSignals();
}

void DatabaseGridView::SetIsRelational(bool rel)
{
    isRelational = rel;
    isProcedure = !rel;
}

void DatabaseGridView::SetIsProcedure(bool isproc)
{
    isProcedure = isproc;
    isRelational = !isproc;
}

void DatabaseGridView::SetDatabaseObjectName(QString objname)
{
    dbObjectName = objname;
}

bool DatabaseGridView::GetIsRelational()
{
    return isRelational;
}

bool DatabaseGridView::GetIsProcedure()
{
    return isProcedure;
}

QString DatabaseGridView::GetDatabaseObjectName()
{
    return dbObjectName;
}

void DatabaseGridView::retranslateUi()
{
    btnConfig->setToolTip(tr("Config columns"));
    btnShowfullscreen->setToolTip(tr("Show Grid in a new window"));
}

void DatabaseGridView::showConfigButtons()
{
    btnConfig->show();
    btnShowfullscreen->show();
}

void DatabaseGridView::hideConfigButtons()
{
    btnConfig->hide();
    btnShowfullscreen->hide();
}
