#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include <QtWidgets/QPushButton>

class ImageButton : public QPushButton
{
    Q_OBJECT
public:

   explicit ImageButton(QWidget *parent = 0);
   //~ImageButton();

protected:

   void paintEvent(QPaintEvent* event);

signals:

public slots:

};

#endif // IMAGEBUTTON_H
