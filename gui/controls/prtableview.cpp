#include "prtableview.h"
#include <QtWidgets/QStyleOption>
#include <QPainter>

PRTableView::PRTableView(QWidget * parent) : QTableView(parent)
{

}

/*PRTableView::~PRTableView()
{

}*/

void PRTableView::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}
