#ifndef SUBNAVIGATION_H
#define SUBNAVIGATION_H

#include <QtWidgets/QWidget>

class SubNavigation : public QWidget
{
    Q_OBJECT

public:

    explicit SubNavigation(QWidget *parent);
    //~SubNavigation();

protected:

    void paintEvent(QPaintEvent* event);

private:

    void setupUi();
};

#endif // SUBNAVIGATION_H
