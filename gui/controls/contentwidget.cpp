#include "contentwidget.h"
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>

ContentWidget::ContentWidget(QWidget *parent) :
    QWidget(parent)
{

}

/*ContentWidget::~ContentWidget()
{

}*/

void ContentWidget::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

void ContentWidget::setupUi()
{

}
