#ifndef DATABASEGRIDVIEW_H
#define DATABASEGRIDVIEW_H

#include <QtWidgets/QWidget>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQueryModel>
//#include "prtableview.h"

class QTableView;
class QVBoxLayout;
class QHBoxLayout;
class ImageButton;
class QSpacerItem;
class FullScreenDatabaseGridView;
class PRTableView;

class DatabaseGridView : public QWidget
{
    Q_OBJECT
public:

    explicit DatabaseGridView(QWidget *parent = 0);
    //~DatabaseGridView();

    void SetIsRelational(bool rel = false);

    void SetIsProcedure(bool isproc = false);

    void SetDatabaseObjectName(QString objname);

    bool GetIsRelational();

    bool GetIsProcedure();

    QString GetDatabaseObjectName();

    void showConfigButtons();

    void hideConfigButtons();

    void retranslateUi();


private:

    void setupUi();

    void connectSignals();

    PRTableView * tableView;

    QSqlQueryModel  sqlModel;
    QSqlDatabase db;

    QHBoxLayout * mainLayout;
    QHBoxLayout * tableLayout;
    QVBoxLayout * btnLayout;
    QSpacerItem * spacerbtn;

    ImageButton * btnConfig;
    ImageButton * btnShowfullscreen;

    FullScreenDatabaseGridView * fullGrid;

    bool isRelational;
    bool isProcedure;

    QString dbObjectName;

protected:

    void paintEvent(QPaintEvent* event);

signals:

public slots:

    void showFull();

};

#endif // DATABASEGRIDVIEW_H
