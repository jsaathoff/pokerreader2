#ifndef PLAYERSELECTOR_H
#define PLAYERSELECTOR_H

#include <QtWidgets/QWidget>

class DatabaseGridView;
class QVBoxLayout;

class PlayerSelector : public QWidget
{
    Q_OBJECT
public:

    explicit PlayerSelector(QWidget *parent = 0);
    //~PlayerSelector();

    DatabaseGridView * playerGrid;

private:

    void setupUi();
    QVBoxLayout * mainLayout;

protected:

    void paintEvent(QPaintEvent* event);

signals:

public slots:

};

#endif // PLAYERSELECTOR_H
