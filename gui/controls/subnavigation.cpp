#include "subnavigation.h"
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>

SubNavigation::SubNavigation(QWidget *parent) : QWidget(parent)
{
    setupUi();
}

/*SubNavigation::~SubNavigation()
{

}*/

void SubNavigation::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}


void SubNavigation::setupUi()
{

}
