#include "playerselector.h"
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>
#include <QtWidgets/QVBoxLayout>
#include "databasegridview.h"

PlayerSelector::PlayerSelector(QWidget *parent) :
    QWidget(parent)
{
    setupUi();
}

/*PlayerSelector::~PlayerSelector()
{

}*/

void PlayerSelector::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

void PlayerSelector::setupUi()
{
    mainLayout = new QVBoxLayout(this);
    playerGrid = new DatabaseGridView(this);
    mainLayout->addWidget(playerGrid, 1);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);
}
