#ifndef MAINNAVIGATION_H
#define MAINNAVIGATION_H

#include <QtWidgets/QWidget>

class NavigationButton;
class QHBoxLayout;

class MainNavigation : public QWidget
{
    Q_OBJECT

public:

    explicit MainNavigation(QWidget *parent);
    //~MainNavigation();

    void retranslateUi();

protected:

    void paintEvent(QPaintEvent* event);

private:

    void setupUi();

    void connectSignals();

    NavigationButton * btnHudOn;

    NavigationButton * btnHudOff;

    NavigationButton * btnOverview;

    NavigationButton * btnSession;

    NavigationButton * btnDetails;

    NavigationButton * btnHands;

    NavigationButton * btnImport;

    NavigationButton * btnGraph;

    QHBoxLayout * layout;

public slots:

    void hideHUD();

    void showHUD();

};

#endif // MAINNAVIGATION_H
