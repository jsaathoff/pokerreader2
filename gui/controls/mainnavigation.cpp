#include "mainnavigation.h"
#include "navigationbutton.h"

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>

MainNavigation::MainNavigation(QWidget *parent) : QWidget(parent)
{
    setupUi();
}

/*MainNavigation::~MainNavigation()
{

}*/

void MainNavigation::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    painter.setRenderHint(QPainter::TextAntialiasing);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

// ui initiation
void MainNavigation::setupUi()
{
    layout = new QHBoxLayout(this);

    btnHudOn = new NavigationButton(this);
    btnHudOn->setObjectName("NavHudOnButton");

    btnHudOff = new NavigationButton(this);
    btnHudOff->setObjectName("NavHudOffButton");

    btnOverview = new NavigationButton(this);
    btnOverview->setObjectName("NavOverviewButton");

    btnDetails = new NavigationButton(this);
    btnDetails->setObjectName("NavDetailsButton");

    btnSession = new NavigationButton(this);
    btnSession->setObjectName("NavSessionsButton");

    btnHands = new NavigationButton(this);
    btnHands->setObjectName("NavHandsButton");

    btnGraph = new NavigationButton(this);
    btnGraph->setObjectName("NavGraphButton");

    btnImport = new NavigationButton(this);
    btnImport->setObjectName("NavImportButton");

    layout->addWidget(btnHudOn, 0, Qt::AlignCenter);
    layout->addWidget(btnHudOff, 0, Qt::AlignCenter);
    layout->addWidget(btnOverview, 0, Qt::AlignCenter);
    layout->addWidget(btnSession, 0, Qt::AlignCenter);
    layout->addWidget(btnDetails, 0, Qt::AlignCenter);
    layout->addWidget(btnGraph, 0, Qt::AlignCenter);
    layout->addWidget(btnHands, 0, Qt::AlignCenter);
    layout->addWidget(btnImport, 0, Qt::AlignCenter);

    retranslateUi();
        setLayout(layout);

    connectSignals();
}

void MainNavigation::retranslateUi()
{
    btnHudOn->SetNavigationText(tr("HUD active"));
    btnHudOff->SetNavigationText(tr("HUD inactive"));
    btnOverview->SetNavigationText(tr("Overview"));
    btnDetails->SetNavigationText(tr("Details"));
    btnSession->SetNavigationText(tr("Sessions"));
    btnHands->SetNavigationText(tr("Hands"));
    btnImport->SetNavigationText(tr("Import"));
    btnGraph->SetNavigationText(tr("Graph"));
}

void MainNavigation::connectSignals()
{
    connect(btnHudOn, SIGNAL(clicked()), this, SLOT(hideHUD()));
    connect(btnHudOff, SIGNAL(clicked()), this, SLOT(showHUD()));
}

void MainNavigation::hideHUD()
{
    btnHudOn->hide();
    btnHudOff->show();
}

void MainNavigation::showHUD()
{
    btnHudOn->show();
    btnHudOff->hide();
}
