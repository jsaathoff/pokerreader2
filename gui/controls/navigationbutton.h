#ifndef NAVIGATIONBUTTON_H
#define NAVIGATIONBUTTON_H

#include <QtWidgets/QWidget>

class QLabel;
class QVBoxLayout;
class QHBoxLayout;

class NavigationButton : public QWidget
{
    Q_OBJECT
public:

    explicit NavigationButton(QWidget *parent = 0);

    //~NavigationButton();

    void SetNavigationText(QString text);

    QString GetNavigationText();

protected:

    void paintEvent(QPaintEvent * event);

    void mousePressEvent(QMouseEvent * event);

private:

    void setupUi();

    QLabel * innerImage;

    QLabel * innerText;

    QVBoxLayout * mainlayout;

signals:

    void clicked();

public slots:

};

#endif // NAVIGATIONBUTTON_H
