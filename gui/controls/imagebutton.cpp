#include "imagebutton.h"
#include <QtWidgets/QStyleOption>
#include <QPainter>

ImageButton::ImageButton(QWidget *parent) :
    QPushButton(parent)
{
}

/*ImageButton::~ImageButton()
{

}*/

void ImageButton::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}
