#include "navigationbutton.h"

#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QStyleOption>
#include <QPainter>

NavigationButton::NavigationButton(QWidget *parent) : QWidget(parent)
{
    setupUi();
}

/*NavigationButton::~NavigationButton()
{

}*/

void NavigationButton::setupUi()
{
    mainlayout = new QVBoxLayout(this);
    innerImage = new QLabel(this);
    innerText = new QLabel(this);

    innerImage->setObjectName("MainNavigationImage");
    innerText->setObjectName("MainNavigationText");

    mainlayout->addWidget(innerImage, 0, Qt::AlignCenter);
    mainlayout->addWidget(innerText, 0, Qt::AlignCenter);

    setObjectName("MainNavigationButton");
    setLayout(mainlayout);
}

void NavigationButton::SetNavigationText(QString text)
{
    innerText->setText(text);
}

QString NavigationButton::GetNavigationText()
{
    return innerText->text();
}

void NavigationButton::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    painter.setRenderHint(QPainter::TextAntialiasing);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

void NavigationButton::mousePressEvent(QMouseEvent * event)
{
    emit clicked();
}
