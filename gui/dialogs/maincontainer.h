#ifndef MAINCONTAINER_H
#define MAINCONTAINER_H

#include <QtWidgets/QWidget>

class QHBoxLayout;
class QVBoxLayout;
class QPushButton;
class QStackedWidget;

class MainNavigation;
class SubNavigation;
class PlayerSelector;
class ContentWidget;


class MainContainer : public QWidget
{
    Q_OBJECT
public:

    explicit MainContainer(QWidget *parent = 0);

    ~MainContainer();

protected:

    void paintEvent(QPaintEvent* event);

private:

    void setupUi();

    QVBoxLayout * mainLayout;
    QVBoxLayout * navLayout;
    QHBoxLayout * contLayout;
    MainNavigation * mainNav;
    SubNavigation * subNav;
    ContentWidget * contentWidget;
    PlayerSelector * playerSelector;

signals:

public slots:

};

#endif // MAINCONTAINER_H
