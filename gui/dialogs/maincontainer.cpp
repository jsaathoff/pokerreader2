#include "maincontainer.h"
#include "../controls/subnavigation.h"
#include "../controls/mainnavigation.h"
#include "../controls/contentwidget.h"
#include "../controls/playerselector.h"
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QStyleOption>
#include <QPainter>


MainContainer::MainContainer(QWidget *parent) :
    QWidget(parent)
{
    setupUi();
}

MainContainer::~MainContainer()
{

}

// ui initiation
void MainContainer::setupUi()
{
    mainNav = new MainNavigation(this);
    subNav = new SubNavigation(this);
    playerSelector = new PlayerSelector(this);
    contentWidget = new ContentWidget(this);

    mainLayout = new QVBoxLayout(this);
    navLayout = new QVBoxLayout(this);
    contLayout = new QHBoxLayout(this);

    navLayout->addWidget(mainNav, 0);
    navLayout->addWidget(subNav, 0, Qt::AlignTop);
    navLayout->setSizeConstraint(QVBoxLayout::SetMaximumSize);

    contLayout->addWidget(playerSelector, 1);
    contLayout->addWidget(contentWidget, 3);

    mainLayout->addLayout(navLayout, 0);
    mainLayout->addLayout(contLayout, 1);

    mainLayout->setContentsMargins(0,0,0,0);

    mainNav->hideHUD();
    setLayout(mainLayout);
}

void MainContainer::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

