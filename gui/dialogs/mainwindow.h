#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QApplication>


class MainContainer;
class QMenuBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:

    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

protected:

    void paintEvent(QPaintEvent* event);

private:

    void setupUi();

    void loadStyleSheet(const QString &stylename);

    void loadFonts(const QString &stylename);

    MainContainer * container;

    QMenuBar * menuBar;

signals:

public slots:

};

#endif // MAINWINDOW_H
