#include "mainwindow.h"
#include "maincontainer.h"
#include <QFile>
#include <QtWidgets/QStyleOption>
#include <QPainter>
#include <QtWidgets/QMenuBar>
#include <QDir>
#include <QFontDatabase>
#include <QtWidgets/QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setupUi();
}

MainWindow::~MainWindow()
{

}

// ui initiation
void MainWindow::setupUi()
{
    container = new MainContainer(this);
    menuBar = new QMenuBar(this);
    setCentralWidget(container);
    loadStyleSheet("default");
}

void MainWindow::paintEvent(QPaintEvent* event)
{
    QStyleOption styleOption;
    styleOption.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &styleOption, &painter, this);
    QWidget::paintEvent(event);
}

void MainWindow::loadStyleSheet(const QString &stylename)
{
    QString filename;
    filename = QApplication::applicationDirPath() + "/styles/" + stylename + "/" + stylename + ".qss";
    QFile file(filename);
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());

    // load all fonts in style
    loadFonts(stylename);

    // and finally set the stylesheet
    qApp->setStyleSheet(styleSheet);
}

void MainWindow::loadFonts(const QString &stylename)
{
    QDir fontDir;
    QString dirName;
    QStringList list;
    QStringList list2;
    QFile fontFile;
    int i, fontID;

    dirName = QApplication::applicationDirPath() + "/styles/" + stylename + "/fonts/";
    fontDir.setPath(dirName);

    list.append("*.ttf");
    list.append("*.otf");

    list2 = fontDir.entryList(list, QDir::Files);	// filter only c++ files

    for(i=0; i<list2.count(); i++)
    {
        fontFile.setFileName(fontDir.path() + "/" + list2.at(i));
        if(fontFile.open(QFile::ReadOnly))
        {
            fontID = -1;
            fontID = QFontDatabase::addApplicationFontFromData(fontFile.readAll());
            if (fontID == -1)
            {
                QMessageBox::warning(0, "Application", (QString)"Impossible d'ouvrir la police " + QChar(0x00AB) + " DejaVu Serif " + QChar(0x00BB) + ".");
            }
        }
    }
}
