#include "fullscreengridview.h"
#include <QtWidgets/QVBoxLayout>
#include "../controls/databasegridview.h"

FullScreenDatabaseGridView::FullScreenDatabaseGridView(QWidget *parent) :
    QDialog(parent)
{
    setupUi();
}

FullScreenDatabaseGridView::~FullScreenDatabaseGridView()
{

}

void FullScreenDatabaseGridView::setupUi()
{
    mainLayout = new QVBoxLayout(this);
    setModal(true);
    hide();

    dbGridView = NULL;

    connectSignals();
}

void FullScreenDatabaseGridView::setGridView(DatabaseGridView * gridview)
{
    if(dbGridView == NULL)
    {
        dbGridView = gridview;
        mainLayout->addWidget(dbGridView);
    }
    else if( (dbGridView != gridview) && ( (dbGridView != NULL) || (gridview != NULL) ) )
        dbGridView = gridview;
}

void FullScreenDatabaseGridView::connectSignals()
{
    connect(this, SIGNAL(close()), this, SLOT(hideWindow()));
}

void FullScreenDatabaseGridView::hideWindow()
{
    hide();
}
