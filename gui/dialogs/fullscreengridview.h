#ifndef FULLSCREENDATABASEGRIDVIEW_H
#define FULLSCREENDATABASEGRIDVIEW_H

#include <QtWidgets/QDialog>
class DatabaseGridView;
class QVBoxLayout;

class FullScreenDatabaseGridView : public QDialog
{
    Q_OBJECT
public:

    explicit FullScreenDatabaseGridView(QWidget *parent = 0);


    ~FullScreenDatabaseGridView();

    void setGridView(DatabaseGridView * gridview);

private:

    void setupUi();

    void connectSignals();

    QVBoxLayout * mainLayout;
    DatabaseGridView * dbGridView;

signals:

public slots:

    void hideWindow();

};

#endif // FULLSCREENDATABASEGRIDVIEW_H
