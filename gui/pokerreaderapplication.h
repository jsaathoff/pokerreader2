#ifndef POKERREADERAPPLICATION_H
#define POKERREADERAPPLICATION_H

#include <QtWidgets/QApplication>
#include <QtWidgets/QSplashScreen>

class ApplicationPaths;

class PokerReaderApplication : public QApplication
{

public:
    PokerReaderApplication(int & argc, char ** argv);

    //! checks if necessary directories exist and creates the directories if not
    bool CheckAndCreateDirectories();

    //! checks if necessary files exist and creates the files if not
    bool CheckAndCreateFiles();

private:

    void connectSignals();

    ApplicationPaths * AppPaths;

signals:

public slots:

};

#endif // POKERREADERAPPLICATION_H
