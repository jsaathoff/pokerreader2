#include <QtWidgets/QApplication>
#include "dialogs/mainwindow.h"
#include "../AppLib/applicationpaths.h"
#include "pokerreaderapplication.h"
#include <QObject>
#include "../AppLib/settings.h"
#include <QtWidgets/QSplashScreen>

int main(int argc, char *argv[])
{

    PokerReaderApplication a(argc, argv);
    MainWindow window;
    QSplashScreen splash(&window, QPixmap(":/images/resources/splash.png"), 0);

    splash.show();
    splash.showMessage(Settings::Instance()->applicationName(), Qt::AlignBottom | Qt::AlignCenter);
    splash.showMessage(QObject::tr("Initialisiere Verzeichnisse..."), Qt::AlignBottom | Qt::AlignCenter);
    a.CheckAndCreateDirectories();
    splash.showMessage(QObject::tr("�berpr�fe und kopiere Dateien..."), Qt::AlignBottom | Qt::AlignCenter);
    a.CheckAndCreateFiles();
    splash.showMessage(QObject::tr("MOIN3"), Qt::AlignBottom | Qt::AlignCenter);

    //qApp->processEvents();

    //ApplicationPaths paths;
    //paths.CheckAndCreateDirectories();


    window.show();
    splash.finish(&window);

    return a.exec();
}


