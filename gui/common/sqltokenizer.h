#ifndef SQLTOKENIZER_H
#define SQLTOKENIZER_H

#include <QObject>
#include <map>
#include <QString>
#include <QStringList>

enum SqlTokenType {
	/*
	 * these are token types used by SqlTokenizer class
	 */
	tkEOF, tkUNKNOWN, tkWHITESPACE, tkCOMMENT,
	tkTERM, tkPARENOPEN, tkPARENCLOSE, tkEQUALS, tkCOMMA,
	tkSTRING, tkIDENTIFIER,

	tk_KEYWORDS_START_HERE,
	/*
	 * SQL keywords start here, these are returned instead of tkIDENTIFIER
	 */
	kwABS, kwACTION, kwACTIVE, kwADD, kwADMIN, kwAFTER, kwALL, kwALTER,
	kwAND, kwANY, kwAS, /*kwASC,*/ kwASCENDING, kwAT, kwAUTO, kwAUTODDL, kwAVG,

	kwBASED, kwBASENAME, kwBASE_NAME, kwBEFORE, kwBEGIN, kwBETWEEN,
	kwBIGINT, kwBIT_LENGTH, kwBLOB, kwBLOBEDIT, kwBOOLEAN, kwBOTH, kwBREAK,
	kwBUFFER, kwBY,

	kwCACHE, kwCASCADE, kwCASE, kwCAST, kwCHAR, kwCHARACTER,
	kwCHARACTER_LENGTH, kwCHAR_LENGTH, kwCHECK, kwCHECK_POINT_LEN,
	kwCHECK_POINT_LENGTH, kwCLOSE, kwCOALESCE, kwCOLLATE, kwCOLLATION,
	kwCOLUMN, kwCOMMENT, kwCOMMIT, kwCOMMITTED, kwCOMPILETIME, kwCOMPUTED,
	kwCONDITIONAL, kwCONNECT, kwCONSTRAINT, kwCONTAINING, kwCONTINUE,
	kwCOUNT, kwCREATE, kwCROSS, kwCSTRING, kwCURRENT, kwCURRENT_CONNECTION,
	kwCURRENT_DATE, kwCURRENT_ROLE, kwCURRENT_TIME, kwCURRENT_TIMESTAMP,
	kwCURRENT_TRANSACTION, kwCURRENT_USER, kwCURSOR,

	kwDATABASE, kwDATE, kwDAY, kwDB_KEY, kwDEBUG, kwDEC, kwDECIMAL,
	kwDECLARE, kwDEFAULT, kwDELETE, kwDELETING, /*kwDESC,*/ kwDESCENDING,
	kwDESCRIBE, kwDESCRIPTOR, kwDISCONNECT, kwDISPLAY, kwDISTINCT, kwDO,
	kwDOMAIN, kwDOUBLE, kwDROP,

	kwECHO, kwEDIT, kwELSE, kwEND, kwENTRY_POINT, kwESCAPE, kwEVENT,
	kwEXCEPTION, kwEXECUTE, kwEXISTS, kwEXIT, kwEXTERN, kwEXTERNAL,
	kwEXTRACT,

	kwFALSE, kwFETCH, kwFILE, kwFILTER, kwFIRST, kwFLOAT, kwFOR,
	kwFOREIGN, kwFOUND, kwFREE_IT, kwFROM, kwFULL, kwFUNCTION,

	kwGDSCODE, kwGENERATOR, kwGEN_ID, kwGLOBAL, kwGOTO, kwGRANT, kwGROUP,
	kwGROUP_COMMIT_, kwGROUP_COMMIT_WAIT,

	kwHAVING, kwHELP, kwHOUR,

	kwIF, kwIIF, kwIMMEDIATE, kwIN, kwINACTIVE, kwINDEX, kwINDICATOR,
	kwINIT, kwINNER, kwINPUT, kwINPUT_TYPE, kwINSERT, kwINSERTING, kwINT,
	kwINTEGER, kwINTO, kwIS, kwISOLATION, kwISQL,

	kwJOIN,
	kwKEY,

	kwLAST, kwLC_MESSAGES, kwLC_TYPE, kwLEADING, kwLEAVE, kwLEFT, kwLENGTH,
	kwLEV, kwLEVEL, kwLIKE, kwLOCK, kwLOGFILE, kwLOG_BUFFER_SIZE,
	kwLOG_BUF_SIZE, kwLONG, kwLOWER,

	kwMANUAL, kwMAX, kwMAXIMUM, kwMAXIMUM_SEGMENT, kwMAX_SEGMENT, kwMERGE,
	kwMESSAGE, kwMIN, kwMINIMUM, kwMINUTE, kwMODULE_NAME, kwMONTH,

	kwNAMES, kwNATIONAL, kwNATURAL, kwNCHAR, kwNO, kwNOAUTO, kwNOT, kwNULL,
	kwNULLIF, kwNULLS, kwNUMERIC, kwNUM_LOG_BUFFERS, kwNUM_LOG_BUFS,

	kwOCTET_LENGTH, kwOF, kwON, kwONLY, kwOPEN, kwOPTION, kwOR, kwORDER,
	kwOUTER, kwOUTPUT, kwOUTPUT_TYPE, kwOVERFLOW,

	kwPAGE, kwPAGELENGTH, kwPAGES, kwPAGE_SIZE, kwPARAMETER, kwPASSWORD,
	kwPERCENT, kwPLAN, kwPOSITION, kwPOST_EVENT, kwPRECISION, kwPREPARE,
	kwPRESERVE, kwPRIMARY, kwPRIVILEGES, kwPROCEDURE, kwPROTECTED, kwPUBLIC,

	kwQUIT,

	kwRAW_PARTITIONS, kwREAD, kwREAL, kwRECORD_VERSION, kwRECREATE,
	kwREFERENCES, kwRELEASE, kwRESERV, kwRESERVING, kwRESTRICT, kwRETAIN,
	kwRETURN, kwRETURNING_VALUES, kwRETURNS, kwREVOKE, kwRIGHT, kwROLE,
	kwROLLBACK, kwROWS, kwROW_COUNT, kwRUNTIME,

	kwSAVEPOINT, kwSCHEMA, kwSECOND, kwSEGMENT, kwSELECT, kwSET, kwSHADOW,
	kwSHARED, kwSHELL, kwSHOW, kwSINGULAR, kwSIZE, kwSKIP, kwSMALLINT,
	kwSNAPSHOT, kwSOME, kwSORT, kwSQLCODE, kwSQLERROR, kwSQLWARNING,
	kwSTABILITY, kwSTARTING, kwSTARTS, kwSTATEMENT, kwSTATIC, kwSTATISTICS,
	kwSUBSTRING, kwSUB_TYPE, kwSUM, kwSUSPEND,

	kwTABLE, kwTEMPORARY, kwTERMINATOR, kwTHEN, kwTIES, kwTIME, kwTIMESTAMP,
	kwTO, kwTRAILING, kwTRANSACTION, kwTRANSLATE, kwTRANSLATION, kwTRIGGER,
	kwTRIM, kwTRUE, kwTYPE,

	kwUNCOMMITTED, kwUNION, kwUNIQUE, kwUNKNOWN, kwUPDATE, kwUPDATING,
	kwUPPER, kwUSER, kwUSING,

	kwVALUE, kwVALUES, kwVARCHAR, kwVARIABLE, kwVARYING, kwVERSION, kwVIEW,

	kwWAIT, kwWAIT_TIME, kwWEEKDAY, kwWHEN, kwWHENEVER, kwWHERE, kwWHILE,
	kwWITH, kwWORK, kwWRITE,

	kwYEAR, kwYEARDAY
};

enum StatementState {

	STINIT,
	STCOMMENTGOTBEGIN,
	STCOMMENTBEGIN,
	STCOMMENTGOTEND,
	STCOMMENTEND,
	STQOUTEBEGIN,
	STQOUTEEND

};

typedef std::map<QString, SqlTokenType> KeywordType;

class SqlTokenizer : public QObject
{
	Q_OBJECT
public:

	explicit SqlTokenizer(QObject *parent = 0, QString statement = "");

	void setSqlStatement(QString statement="");

	void initKeyWords();

	void buildSqlStatements();

	void setTerm(QChar terminator);

	int getKeywordType(QString word);

	void resetMultilineComment();

	void resetLineComment();

	void resetQoutedString();

	bool gotKeyWord();

	void resetTokenizer();

private:
	QChar term;
	const QChar * sqlStart;
	const QChar * sqlEnd;
	QChar commentToken;
	QChar qouteToken;
	QChar donoteat;

	QString sqlComplete;

	QStringList statements;

	KeywordType keywordmap;

	bool foundKeyword;
	int slcomment;
	int mlcomment;
	int qoutedstring;
	bool gotterm;
	bool gotend;

signals:

public slots:

};

#endif // SQLTOKENIZER_H
