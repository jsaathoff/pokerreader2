#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>

class QSqlDatabase;
class QSqlQueryModel;

class Database : public QObject
{
	Q_OBJECT
public:

	explicit Database(QObject *parent = 0);

	bool Connect();

private:
	QSqlDatabase * db;
	QSqlQueryModel * model;

	QString username;
	QString password;

signals:

public slots:

};

#endif // DATABASE_H
