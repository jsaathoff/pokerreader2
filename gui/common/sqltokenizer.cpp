#include "sqltokenizer.h"

SqlTokenizer::SqlTokenizer(QObject *parent, QString statement) :
	QObject(parent)
{
	term = ';';

	foundKeyword = false;
	gotterm = false;
	mlcomment = STINIT;
	slcomment = STINIT;
	qoutedstring = STINIT;
	gotterm = false;
	gotend = false;

	setSqlStatement(statement);

	initKeyWords();
}

void SqlTokenizer::setSqlStatement(QString statement)
{
	sqlComplete = statement;
}

void SqlTokenizer::initKeyWords()
{
	keywordmap["ABS"] = kwABS;
	keywordmap["ACTION"] = kwACTION;
	keywordmap["ACTIVE"] = kwACTIVE;
	keywordmap["ADD"] = kwADD;
	keywordmap["ADMIN"] = kwADMIN;
	keywordmap["AFTER"] = kwAFTER;
	keywordmap["ALL"] = kwALL;
	keywordmap["ALTER"] = kwALTER;
	keywordmap["AND"] = kwAND;
	keywordmap["ANY"] = kwANY;
	keywordmap["AS"] = kwAS;
	keywordmap["ASC"] = kwASCENDING;
	keywordmap["ASCENDING"] = kwASCENDING;
	keywordmap["AT"] = kwAT;
	keywordmap["AUTO"] = kwAUTO;
	keywordmap["AUTODDL"] = kwAUTODDL;
	keywordmap["AVG"] = kwAVG;
	// TODO keywordmap["BACKUP"] = kwBACKUP;            // fb2.0
	keywordmap["BASED"] = kwBASED;
	keywordmap["BASENAME"] = kwBASENAME;
	keywordmap["BASE_NAME"] = kwBASE_NAME;
	keywordmap["BEFORE"] = kwBEFORE;
	keywordmap["BEGIN"] = kwBEGIN;
	keywordmap["BETWEEN"] = kwBETWEEN;
	keywordmap["BIGINT"] = kwBIGINT;
	keywordmap["BIT_LENGTH"] = kwBIT_LENGTH;    // fb2.0
	keywordmap["BLOB"] = kwBLOB;
	keywordmap["BLOBEDIT"] = kwBLOBEDIT;
	// TODO keywordmap["BLOCK"] = kwBLOCK;              // fb2.0
	keywordmap["BOOLEAN"] = kwBOOLEAN;
	keywordmap["BOTH"] = kwBOTH;                // fb2.0
	keywordmap["BREAK"] = kwBREAK;
	keywordmap["BUFFER"] = kwBUFFER;
	keywordmap["BY"] = kwBY;
	keywordmap["CACHE"] = kwCACHE;
	keywordmap["CASCADE"] = kwCASCADE;
	keywordmap["CASE"] = kwCASE;
	keywordmap["CAST"] = kwCAST;
	keywordmap["CHAR"] = kwCHAR;                // fb2.0
	keywordmap["CHARACTER"] = kwCHARACTER;
	keywordmap["CHARACTER_LENGTH"] = kwCHARACTER_LENGTH;    // fb2.0
	keywordmap["CHAR_LENGTH"] = kwCHAR_LENGTH;
	keywordmap["CHECK"] = kwCHECK;
	keywordmap["CHECK_POINT_LEN"] = kwCHECK_POINT_LEN;
	keywordmap["CHECK_POINT_LENGTH"] = kwCHECK_POINT_LENGTH;
	keywordmap["CLOSE"] = kwCLOSE;                              // fb2.0
	keywordmap["COALESCE"] = kwCOALESCE;
	keywordmap["COLLATE"] = kwCOLLATE;
	keywordmap["COLLATION"] = kwCOLLATION;
	keywordmap["COLUMN"] = kwCOLUMN;
	keywordmap["COMMENT"] = kwCOMMENT;
	keywordmap["COMMIT"] = kwCOMMIT;
	keywordmap["COMMITTED"] = kwCOMMITTED;
	keywordmap["COMPILETIME"] = kwCOMPILETIME;
	keywordmap["COMPUTED"] = kwCOMPUTED;
	keywordmap["CONDITIONAL"] = kwCONDITIONAL;
	keywordmap["CONNECT"] = kwCONNECT;
	keywordmap["CONSTRAINT"] = kwCONSTRAINT;
	keywordmap["CONTAINING"] = kwCONTAINING;
	keywordmap["CONTINUE"] = kwCONTINUE;
	keywordmap["COUNT"] = kwCOUNT;
	keywordmap["CREATE"] = kwCREATE;
	keywordmap["CROSS"] = kwCROSS;          // fb2.0
	keywordmap["CSTRING"] = kwCSTRING;
	keywordmap["CURRENT"] = kwCURRENT;
	keywordmap["CURRENT_CONNECTION"] = kwCURRENT_CONNECTION;
	keywordmap["CURRENT_DATE"] = kwCURRENT_DATE;
	keywordmap["CURRENT_ROLE"] = kwCURRENT_ROLE;
	keywordmap["CURRENT_TIME"] = kwCURRENT_TIME;
	keywordmap["CURRENT_TIMESTAMP"] = kwCURRENT_TIMESTAMP;
	keywordmap["CURRENT_TRANSACTION"] = kwCURRENT_TRANSACTION;
	keywordmap["CURRENT_USER"] = kwCURRENT_USER;
	keywordmap["CURSOR"] = kwCURSOR;
	keywordmap["DATABASE"] = kwDATABASE;
	keywordmap["DATE"] = kwDATE;
	keywordmap["DAY"] = kwDAY;
	keywordmap["DB_KEY"] = kwDB_KEY;
	keywordmap["DEBUG"] = kwDEBUG;
	keywordmap["DEC"] = kwDEC;
	keywordmap["DECIMAL"] = kwDECIMAL;
	keywordmap["DECLARE"] = kwDECLARE;
	keywordmap["DEFAULT"] = kwDEFAULT;
	keywordmap["DELETE"] = kwDELETE;
	keywordmap["DELETING"] = kwDELETING;
	keywordmap["DESC"] = kwDESCENDING;
	keywordmap["DESCENDING"] = kwDESCENDING;
	keywordmap["DESCRIBE"] = kwDESCRIBE;
	keywordmap["DESCRIPTOR"] = kwDESCRIPTOR;
	// TODO keywordmap["DIFFERENCE"] = kwDIFFERENCE; // fb2.0
	keywordmap["DISCONNECT"] = kwDISCONNECT;
	keywordmap["DISPLAY"] = kwDISPLAY;
	keywordmap["DISTINCT"] = kwDISTINCT;
	keywordmap["DO"] = kwDO;
	keywordmap["DOMAIN"] = kwDOMAIN;
	keywordmap["DOUBLE"] = kwDOUBLE;
	keywordmap["DROP"] = kwDROP;
	keywordmap["ECHO"] = kwECHO;
	keywordmap["EDIT"] = kwEDIT;
	keywordmap["ELSE"] = kwELSE;
	keywordmap["END"] = kwEND;
	keywordmap["ENTRY_POINT"] = kwENTRY_POINT;
	keywordmap["ESCAPE"] = kwESCAPE;
	keywordmap["EVENT"] = kwEVENT;
	keywordmap["EXCEPTION"] = kwEXCEPTION;
	keywordmap["EXECUTE"] = kwEXECUTE;
	keywordmap["EXISTS"] = kwEXISTS;
	keywordmap["EXIT"] = kwEXIT;
	keywordmap["EXTERN"] = kwEXTERN;
	keywordmap["EXTERNAL"] = kwEXTERNAL;
	keywordmap["EXTRACT"] = kwEXTRACT;
	keywordmap["FALSE"] = kwFALSE;
	keywordmap["FETCH"] = kwFETCH;              // fb2.0
	keywordmap["FILE"] = kwFILE;
	keywordmap["FILTER"] = kwFILTER;
	keywordmap["FIRST"] = kwFIRST;
	keywordmap["FLOAT"] = kwFLOAT;
	keywordmap["FOR"] = kwFOR;
	keywordmap["FOREIGN"] = kwFOREIGN;
	keywordmap["FOUND"] = kwFOUND;
	keywordmap["FREE_IT"] = kwFREE_IT;
	keywordmap["FROM"] = kwFROM;
	keywordmap["FULL"] = kwFULL;
	keywordmap["FUNCTION"] = kwFUNCTION;
	keywordmap["GDSCODE"] = kwGDSCODE;
	keywordmap["GENERATOR"] = kwGENERATOR;
	keywordmap["GEN_ID"] = kwGEN_ID;
	keywordmap["GLOBAL"] = kwGLOBAL;
	keywordmap["GOTO"] = kwGOTO;
	keywordmap["GRANT"] = kwGRANT;
	keywordmap["GROUP"] = kwGROUP;
	keywordmap["GROUP_COMMIT_"] = kwGROUP_COMMIT_;
	keywordmap["GROUP_COMMIT_WAIT"] = kwGROUP_COMMIT_WAIT;
	keywordmap["HAVING"] = kwHAVING;
	keywordmap["HELP"] = kwHELP;
	keywordmap["HOUR"] = kwHOUR;
	keywordmap["IF"] = kwIF;
	keywordmap["IIF"] = kwIIF;
	keywordmap["IMMEDIATE"] = kwIMMEDIATE;
	keywordmap["IN"] = kwIN;
	keywordmap["INACTIVE"] = kwINACTIVE;
	keywordmap["INDEX"] = kwINDEX;
	keywordmap["INDICATOR"] = kwINDICATOR;
	keywordmap["INIT"] = kwINIT;
	keywordmap["INNER"] = kwINNER;
	keywordmap["INPUT"] = kwINPUT;
	keywordmap["INPUT_TYPE"] = kwINPUT_TYPE;
	keywordmap["INSERT"] = kwINSERT;
	keywordmap["INSERTING"] = kwINSERTING;
	keywordmap["INT"] = kwINT;
	keywordmap["INTEGER"] = kwINTEGER;
	keywordmap["INTO"] = kwINTO;
	keywordmap["IS"] = kwIS;
	keywordmap["ISOLATION"] = kwISOLATION;
	keywordmap["ISQL"] = kwISQL;
	keywordmap["JOIN"] = kwJOIN;
	keywordmap["KEY"] = kwKEY;
	keywordmap["LAST"] = kwLAST;
	keywordmap["LC_MESSAGES"] = kwLC_MESSAGES;
	keywordmap["LC_TYPE"] = kwLC_TYPE;
	keywordmap["LEADING"] = kwLEADING;          // fb2.0
	keywordmap["LEAVE"] = kwLEAVE;
	keywordmap["LEFT"] = kwLEFT;
	keywordmap["LENGTH"] = kwLENGTH;
	keywordmap["LEV"] = kwLEV;
	keywordmap["LEVEL"] = kwLEVEL;
	keywordmap["LIKE"] = kwLIKE;
	keywordmap["LOCK"] = kwLOCK;
	keywordmap["LOGFILE"] = kwLOGFILE;
	keywordmap["LOG_BUFFER_SIZE"] = kwLOG_BUFFER_SIZE;
	keywordmap["LOG_BUF_SIZE"] = kwLOG_BUF_SIZE;
	keywordmap["LONG"] = kwLONG;
	keywordmap["LOWER"] = kwLOWER;              // fb2.0
	keywordmap["MANUAL"] = kwMANUAL;
	keywordmap["MAX"] = kwMAX;
	keywordmap["MAXIMUM"] = kwMAXIMUM;
	keywordmap["MAXIMUM_SEGMENT"] = kwMAXIMUM_SEGMENT;
	keywordmap["MAX_SEGMENT"] = kwMAX_SEGMENT;
	keywordmap["MERGE"] = kwMERGE;
	keywordmap["MESSAGE"] = kwMESSAGE;
	keywordmap["MIN"] = kwMIN;
	keywordmap["MINIMUM"] = kwMINIMUM;
	keywordmap["MINUTE"] = kwMINUTE;
	keywordmap["MODULE_NAME"] = kwMODULE_NAME;
	keywordmap["MONTH"] = kwMONTH;
	keywordmap["NAMES"] = kwNAMES;
	keywordmap["NATIONAL"] = kwNATIONAL;
	keywordmap["NATURAL"] = kwNATURAL;
	keywordmap["NCHAR"] = kwNCHAR;
		// TODO keywordmap["NEXT"] = kwNEXT;
	keywordmap["NO"] = kwNO;
	keywordmap["NOAUTO"] = kwNOAUTO;
	keywordmap["NOT"] = kwNOT;
	keywordmap["NULL"] = kwNULL;
	keywordmap["NULLIF"] = kwNULLIF;
	keywordmap["NULLS"] = kwNULLS;
	keywordmap["NUMERIC"] = kwNUMERIC;
	keywordmap["NUM_LOG_BUFFERS"] = kwNUM_LOG_BUFFERS;
	keywordmap["NUM_LOG_BUFS"] = kwNUM_LOG_BUFS;
	keywordmap["OCTET_LENGTH"] = kwOCTET_LENGTH;        // fb2.0
	keywordmap["OF"] = kwOF;
	keywordmap["ON"] = kwON;
	keywordmap["ONLY"] = kwONLY;
	keywordmap["OPEN"] = kwOPEN;                        // fb2.0
	keywordmap["OPTION"] = kwOPTION;
	keywordmap["OR"] = kwOR;
	keywordmap["ORDER"] = kwORDER;
	keywordmap["OUTER"] = kwOUTER;
	keywordmap["OUTPUT"] = kwOUTPUT;
	keywordmap["OUTPUT_TYPE"] = kwOUTPUT_TYPE;
	keywordmap["OVERFLOW"] = kwOVERFLOW;
	keywordmap["PAGE"] = kwPAGE;
	keywordmap["PAGELENGTH"] = kwPAGELENGTH;
	keywordmap["PAGES"] = kwPAGES;
	keywordmap["PAGE_SIZE"] = kwPAGE_SIZE;
	keywordmap["PARAMETER"] = kwPARAMETER;
	keywordmap["PASSWORD"] = kwPASSWORD;
	keywordmap["PERCENT"] = kwPERCENT;
	keywordmap["PLAN"] = kwPLAN;
	keywordmap["POSITION"] = kwPOSITION;
	keywordmap["POST_EVENT"] = kwPOST_EVENT;
	keywordmap["PRECISION"] = kwPRECISION;
	keywordmap["PREPARE"] = kwPREPARE;
	keywordmap["PRESERVE"] = kwPRESERVE;
	keywordmap["PRIMARY"] = kwPRIMARY;
	keywordmap["PRIVILEGES"] = kwPRIVILEGES;
	keywordmap["PROCEDURE"] = kwPROCEDURE;
	keywordmap["PROTECTED"] = kwPROTECTED;
	keywordmap["PUBLIC"] = kwPUBLIC;
	keywordmap["QUIT"] = kwQUIT;
	keywordmap["RAW_PARTITIONS"] = kwRAW_PARTITIONS;
	keywordmap["READ"] = kwREAD;
	keywordmap["REAL"] = kwREAL;
	keywordmap["RECORD_VERSION"] = kwRECORD_VERSION;
	keywordmap["RECREATE"] = kwRECREATE;
	keywordmap["REFERENCES"] = kwREFERENCES;
	keywordmap["RELEASE"] = kwRELEASE;
	keywordmap["RESERV"] = kwRESERV;
	keywordmap["RESERVING"] = kwRESERVING;
	// TODO keywordmap["RESTART"] = kwRESTART;
	keywordmap["RESTRICT"] = kwRESTRICT;
	keywordmap["RETAIN"] = kwRETAIN;
	keywordmap["RETURN"] = kwRETURN;
	// TODO keywordmap["RETURNING"] = kwRETURNING;
	keywordmap["RETURNING_VALUES"] = kwRETURNING_VALUES;
	keywordmap["RETURNS"] = kwRETURNS;
	keywordmap["REVOKE"] = kwREVOKE;
	keywordmap["RIGHT"] = kwRIGHT;
	keywordmap["ROLE"] = kwROLE;
	keywordmap["ROLLBACK"] = kwROLLBACK;
	keywordmap["ROWS"] = kwROWS;                    // fb2.0
	keywordmap["ROW_COUNT"] = kwROW_COUNT;
	keywordmap["RUNTIME"] = kwRUNTIME;
	keywordmap["SAVEPOINT"] = kwSAVEPOINT;
	// TODO keywordmap["SCALAR_ARRAY"] = kwSCALAR_ARRAY;
	keywordmap["SCHEMA"] = kwSCHEMA;
	keywordmap["SECOND"] = kwSECOND;
	keywordmap["SEGMENT"] = kwSEGMENT;
	keywordmap["SELECT"] = kwSELECT;
	keywordmap["SEQUENCE"] = kwGENERATOR;
	keywordmap["SET"] = kwSET;
	keywordmap["SHADOW"] = kwSHADOW;
	keywordmap["SHARED"] = kwSHARED;
	keywordmap["SHELL"] = kwSHELL;
	keywordmap["SHOW"] = kwSHOW;
	keywordmap["SINGULAR"] = kwSINGULAR;
	keywordmap["SIZE"] = kwSIZE;
	keywordmap["SKIP"] = kwSKIP;
	keywordmap["SMALLINT"] = kwSMALLINT;
	keywordmap["SNAPSHOT"] = kwSNAPSHOT;
	keywordmap["SOME"] = kwSOME;
	keywordmap["SORT"] = kwSORT;
	keywordmap["SQLCODE"] = kwSQLCODE;
	keywordmap["SQLERROR"] = kwSQLERROR;
	keywordmap["SQLWARNING"] = kwSQLWARNING;
	keywordmap["STABILITY"] = kwSTABILITY;
	keywordmap["STARTING"] = kwSTARTING;
	keywordmap["STARTS"] = kwSTARTS;
	keywordmap["STATEMENT"] = kwSTATEMENT;
	keywordmap["STATIC"] = kwSTATIC;
	keywordmap["STATISTICS"] = kwSTATISTICS;
	keywordmap["SUBSTRING"] = kwSUBSTRING;
	keywordmap["SUB_TYPE"] = kwSUB_TYPE;
	keywordmap["SUM"] = kwSUM;
	keywordmap["SUSPEND"] = kwSUSPEND;
	keywordmap["TABLE"] = kwTABLE;
	keywordmap["TEMPORARY"] = kwTEMPORARY;
	keywordmap["TERM"] = kwTERMINATOR;
	keywordmap["TERMINATOR"] = kwTERMINATOR;
	keywordmap["THEN"] = kwTHEN;
	keywordmap["TIES"] = kwTIES;
	keywordmap["TIME"] = kwTIME;
	keywordmap["TIMESTAMP"] = kwTIMESTAMP;
	keywordmap["TO"] = kwTO;
	keywordmap["TRAILING"] = kwTRAILING;            // fb2.0
	keywordmap["TRANSACTION"] = kwTRANSACTION;
	keywordmap["TRANSLATE"] = kwTRANSLATE;
	keywordmap["TRANSLATION"] = kwTRANSLATION;
	keywordmap["TRIGGER"] = kwTRIGGER;
	keywordmap["TRIM"] = kwTRIM;                    // fb2.0
	keywordmap["TRUE"] = kwTRUE;
	keywordmap["TYPE"] = kwTYPE;
	keywordmap["UNCOMMITTED"] = kwUNCOMMITTED;
	keywordmap["UNION"] = kwUNION;
	keywordmap["UNIQUE"] = kwUNIQUE;
	keywordmap["UNKNOWN"] = kwUNKNOWN;
	keywordmap["UPDATE"] = kwUPDATE;
	keywordmap["UPDATING"] = kwUPDATING;
	keywordmap["UPPER"] = kwUPPER;
	keywordmap["USER"] = kwUSER;
	keywordmap["USING"] = kwUSING;
	keywordmap["VALUE"] = kwVALUE;
	keywordmap["VALUES"] = kwVALUES;
	keywordmap["VARCHAR"] = kwVARCHAR;
	keywordmap["VARIABLE"] = kwVARIABLE;
	keywordmap["VARYING"] = kwVARYING;
	keywordmap["VERSION"] = kwVERSION;
	keywordmap["VIEW"] = kwVIEW;
	keywordmap["WAIT"] = kwWAIT;
	keywordmap["WAIT_TIME"] = kwWAIT_TIME;
	keywordmap["WEEKDAY"] = kwWEEKDAY;
	keywordmap["WHEN"] = kwWHEN;
	keywordmap["WHENEVER"] = kwWHENEVER;
	keywordmap["WHERE"] = kwWHERE;
	keywordmap["WHILE"] = kwWHILE;
	keywordmap["WITH"] = kwWITH;
	keywordmap["WORK"] = kwWORK;
	keywordmap["WRITE"] = kwWRITE;
	keywordmap["YEAR"] = kwYEAR;
	keywordmap["YEARDAY"] = kwYEARDAY;
	keywordmap[""] = tkUNKNOWN;
}

void SqlTokenizer::buildSqlStatements()
{
	int i = 0;

	QString sqltmp;
	int end = sqlComplete.length();

	int posbeg = 0, posend = 0;

	QChar c = sqlComplete[i];

	posbeg = i;

	for(i = 0; i <= end; i++)
	{
		c = sqlComplete[i];
		if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) // sql keywords start with a-z or A-Z
		{
			if( (mlcomment < STCOMMENTGOTBEGIN) && (slcomment < STCOMMENTGOTBEGIN))
				sqltmp.append(c);
		}
		else if(c.isSpace()) // scan for space
		{
			if( (qoutedstring < STQOUTEBEGIN) && (slcomment < STCOMMENTGOTBEGIN) && (mlcomment < STCOMMENTGOTBEGIN) )
			{
				if(getKeywordType(sqltmp) > tk_KEYWORDS_START_HERE) // got a keyword? if yes, gotkeyword will be true
					sqltmp.append(" ");
				else if(gotKeyWord())
					sqltmp.append(" ");

			}
		}

		else if(c == '-')
		{
			if(sqlComplete[i+1] == '-' && (slcomment < STCOMMENTGOTBEGIN) && (!gotKeyWord()))
				slcomment = STCOMMENTBEGIN;
			else if(slcomment == STCOMMENTBEGIN)
				slcomment = STCOMMENTEND;
			else
				sqltmp.append(c);
		}
		else if(c == '*')
		{
			if(gotKeyWord() && mlcomment < STCOMMENTGOTBEGIN)
			{
				sqltmp.append(c);
			}
			else
			{
				if(mlcomment == STCOMMENTGOTBEGIN)
				{
					if(sqlComplete[i+1] == '/')
					{
						mlcomment = STCOMMENTBEGIN;
					}
					//else if(mlcomment > STCOMMENTGOTEND)

				}
			}
		}
		else if(c == '/')
		{
			if(gotKeyWord() && mlcomment < STCOMMENTGOTBEGIN)
			{
				sqltmp.append(c);
			}
			else
			{
				if(mlcomment < STCOMMENTGOTBEGIN)
				{
					mlcomment = STCOMMENTGOTBEGIN;

					if(sqlComplete[i+1] == '*')
						mlcomment = STCOMMENTBEGIN;
				}
			}
		}
		else if((c == '\'') || (c == '"'))
		{
			if(qoutedstring < STQOUTEBEGIN)
				qoutedstring = STQOUTEBEGIN;
			else if(qoutedstring == STQOUTEBEGIN)
				qoutedstring = STQOUTEEND;
		}
		else if(c == term)
		{
			if( (qoutedstring > STQOUTEBEGIN ) && (mlcomment < STCOMMENTGOTEND) && (slcomment < STCOMMENTGOTEND))
				statements.append(sqltmp);
			if(gotKeyWord())
			{
				QString tmp = sqltmp.mid(sqltmp.indexOf("TERM"), 4);
				if(tmp.endsWith(term))
				{
					tmp = tmp.trimmed();
					term = (QChar)*tmp.data();
				}
			}
		}
		else if( (c == '\n') || (c == '\r'))
		{
			if(gotKeyWord())
			{
				sqltmp.append(c);
			}
		}
		else if( c == '\0')
		{
			break;
		}
		else
		{
			if(gotKeyWord())
				sqltmp.append(c);
		}
		//else if(sqltmp.indexOf("SET" == "")
		/*
		else
		{
			if(!comment)
				sqltmp.append(c);
		}
		/*else if(c == '/')
		{
			c = sqlComplete[i+1];
			if(c == '*')
			{
				if(!gotKeyWord() && (!beganmlcomment))
				{
					beganmlcomment = true;
				}
				else if(gotKeyWord())
					sqltmp.append(c);
			}
			else if(!gotKeyWord() && (beganmlcomment))
			{
				beganmlcomment = false;
			}
		}
		else if(c == '-')
		{
			if(gotKeyWord())
			{
				sqltmp.append(c);
			}
		}
		else if(c == '\r')
			sqltmp.append(" ");
		else if(c == '\r')
			sqltmp.append(" ");
		else
		{
			if(c == term.data())
				statements.append(sqltmp);
		}
		/*

		else if( (c == '\'') || (c == '"'))
		{
			if(!beganqoutedstring)
			beganqoutedstring = true;
			qouteToken = c;
		}
		else if( (c == '\r') || (c == '\n'))
		{
			if(gotKeyWord())
				sqltmp.append(" ");
		}
		*/
	}

	int di=0;
	di++;
}

void SqlTokenizer::setTerm(QChar terminator)
{
	term = terminator;
}

int SqlTokenizer::getKeywordType(QString word)
{
	KeywordType::const_iterator it = keywordmap.find(word);
	if(it != keywordmap.end())
	{
		if((*it).second > tk_KEYWORDS_START_HERE)
		{
			foundKeyword = true;
			return (*it).second;
		}
	}

	return tkUNKNOWN;
}


void SqlTokenizer::resetMultilineComment()
{

}

void SqlTokenizer::resetLineComment()
{

}

void SqlTokenizer::resetQoutedString()
{

}

bool SqlTokenizer::gotKeyWord()
{
	return foundKeyword;
}

void SqlTokenizer::resetTokenizer()
{
	slcomment = STINIT;
	mlcomment = STINIT;
	qoutedstring = STINIT;
}
