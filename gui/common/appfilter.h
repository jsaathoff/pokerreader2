#ifndef APPFILTER_H
#define APPFILTER_H

#include <QObject>

class AppFilter : public QObject
{
	Q_OBJECT
public:

	explicit AppFilter(QObject *parent = 0);

	~AppFilter();

	void SetPlayerID(qint64 playerid);

	void SetProvider(qint32 provider);

	void SetCurrency(qint32 currency);

	qint64 GetPlayerID();

	qint32 GetProvider();

	qint32 GetCurrency();

private:
	qint64 m_nPlayerID;
	qint32 m_nProvider;
	qint32 m_nCurrency;

signals:

public slots:

};

#endif // APPFILTER_H
