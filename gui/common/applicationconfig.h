#ifndef APPLICATIONCONFIG_H
#define APPLICATIONCONFIG_H

#include <QObject>
class QSettings;
class ApplicationPaths;

class ApplicationConfig : public QObject
{
	Q_OBJECT
public:
	explicit ApplicationConfig(QObject *parent = 0);

	~ApplicationConfig();

	void saveSettings();

	QSettings * Settings();

	ApplicationPaths * paths;

signals:

public slots:

private:
	QSettings * settings;
};

#endif // APPLICATIONCONFIG_H
