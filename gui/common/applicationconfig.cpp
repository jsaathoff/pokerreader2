#include "applicationconfig.h"
#include <QDir>
#include <QSettings>
#include "../../AppLib/applicationpaths.h"

ApplicationConfig::ApplicationConfig(QObject *parent) : QObject(parent)
{
    settings = new QSettings(paths->UserConfigDir() + "pokerreader.ini" , QSettings::IniFormat);
    paths = new ApplicationPaths(this);
}

ApplicationConfig::~ApplicationConfig()
{
    delete settings;
    delete paths;
}

void ApplicationConfig::saveSettings()
{
    settings->sync();
}

QSettings * ApplicationConfig::Settings()
{
    return settings;
}
