#include "appfilter.h"

AppFilter::AppFilter(QObject *parent) :
    QObject(parent)
{

}

AppFilter::~AppFilter()
{

}

void AppFilter::SetPlayerID(qint64 playerid)
{
    this->m_nPlayerID = playerid;
}

void AppFilter::SetProvider(qint32 provider)
{
    this->m_nProvider = provider;
}

void AppFilter::SetCurrency(qint32 currency)
{
    this->m_nCurrency = currency;
}

qint64 AppFilter::GetPlayerID()
{
    return m_nPlayerID;
}

qint32 AppFilter::GetProvider()
{
    return m_nProvider;
}

qint32 AppFilter::GetCurrency()
{
    return m_nCurrency;
}
