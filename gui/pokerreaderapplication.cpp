#include "pokerreaderapplication.h"
#include "../AppLib/applicationpaths.h"

#include <QDir>
#include <QTextStream>
#include <QFileInfo>
#include <QLocale>
#include <QDebug>
#include <iostream>

//#include "parser/pokerstarsparser.h"
#include "../AppLib/applibrary.h"

PokerReaderApplication::PokerReaderApplication(int & argc, char ** argv ) : QApplication(argc, argv)
{
	setApplicationName("PokerReader");
	setOrganizationName("Sharktoolz");

	AppPaths = new ApplicationPaths(this);
	CheckAndCreateDirectories();
	CheckAndCreateFiles();
	//connectSignals();

	//SqlTokenizer tokniz(NULL, "/*comment*/ ALTER TABLE TOURNAMENT_PLAYER ADD CONSTRAINT FK_TOURNAMENT_PLAYER_HAND FOREIGN KEY (HANDID) REFERENCES HANDS (ID);");
	//tokniz.buildSqlStatements();
	//PokerStarsParser pstars(this);
	//pstars.getDouble("1,1");

	//pstars.parse(QString("PokerStars Game #20702615355:  Hold'em No Limit ($0.10/$0.25) - 2008/09/26 4:34:35 ET"));
}

void PokerReaderApplication::connectSignals()
{
	//connect(this, SIGNAL(splashMessage(QString)), this, SLOT(writeSplashMessage(QString)));
}

bool PokerReaderApplication::CheckAndCreateDirectories()
{
	QDir dir;
	QTextStream in;
	QFile file;
	QString folderline;

	dir.mkdir(AppPaths->UserDataDir()); // create user data dir (e.g. C:\Users\yourUser\PokerReader\)

	file.setFileName(":/files/resources/createfolders.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		return false;
	}

	in.setDevice(&file);
	in.setAutoDetectUnicode(true);
	while (!in.atEnd())
	{
		folderline = in.readLine(); // read the line with the directory
		folderline.replace("$USER$", AppPaths->UserDataDir(), Qt::CaseInsensitive); // build complete directory-string $USER$ is the user'S private directory
		//folderline.replace("$APPPATH$", AppPaths->UserDataDir(), Qt::CaseInsensitive); // build complete directory-string $APPPATH$ is the app exe directory
		dir.mkdir(folderline); // make the directory
	}

	return true;
}

bool PokerReaderApplication::CheckAndCreateFiles()
{
	bool result = false;

	QTextStream in;
	QFile file;
	QString fileline, source, target, mode, tmp;
	QFileInfo fileinfo;
	int targetstart = 0, modestart = 0;
	QDir dir;
	bool copyfile = false;

	file.setFileName(":/files/resources/copyfiles.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		result = false;
	}
	else
		result = true;

	if(result)
	{
		in.setDevice(&file);
		in.setAutoDetectUnicode(true);
		while (!in.atEnd())
		{
			fileline = in.readLine(); // read the line with the directory
			if(!fileline.startsWith("####")) // the line does not start with ####, it's not a comment
			{
				targetstart = fileline.indexOf("|TARGET|");
				modestart = fileline.indexOf("|MODE|");

				target = fileline.mid(targetstart, (modestart - targetstart) ); // The target, copy the file to here...
				mode = fileline.mid(modestart, fileline.length() - modestart); // get the copymode
				source = fileline.mid(0,targetstart); // the sourcefile
				source.replace("|SOURCE|", ""); // the source file
				target.replace("|TARGET|", ""); // the target file
				mode.replace("|MODE|", ""); // the mode

				target.replace("$USER$", AppPaths->UserDataDir(), Qt::CaseInsensitive); // build complete directory-string $USER$ is the user'S private directory
				target.replace("$APPPATH$", applicationDirPath(), Qt::CaseInsensitive); // build complete directory-string $APPPATH$ is the app directory

				source.replace("$USER$", AppPaths->UserDataDir(), Qt::CaseInsensitive); // build complete directory-string $USER$ is the user'S private directory
				source.replace("$APPPATH$", applicationDirPath(), Qt::CaseInsensitive); // build complete directory-string $APPPATH$ is the app directory

				if(mode == "copyempty")
				{
					tmp = target.mid(0,target.lastIndexOf("/"));
					dir.setPath(tmp);
					if(dir.entryList(QDir::NoFilter, QDir::NoSort).count() < 1) // no files in dir ?
						copyfile = true;
					else
					copyfile = false;
				}
				else if(mode == "overwrite")
				{
					if(QFile::exists(target)) // file exists
					{
						if(QFile::remove(target)) // yes, remove the file
							copyfile = true;
						else
							copyfile = false;
					}
					else
						copyfile = true;
				}
				else if(mode == "copyifnotexists")
				{
					if(!QFile::exists(target)) // file does not exist
						copyfile = true;
					else
						copyfile = false;
				}

				if(copyfile)
				{
					file.setFileName(source);
					if(file.open(QFile::ReadWrite))
						file.copy(target);
				}
			}
		}
	}


	return result;
}
