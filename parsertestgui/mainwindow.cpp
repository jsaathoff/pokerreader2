#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QDateTime>
#include <QTextCodec>
#include <QDebug>
#include "../AppLib/parser/handimporter.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //re2 = QRegExp("(\\w+)\\s+folds");
    //re = pcrecpp::RE::Compile("\\w");
    //re = new pcrecpp::RE("(\\w+)\\s+folds");
    ui->setupUi(this);
    connectSignals();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectSignals()
{
    connect(ui->pbLoadfile, SIGNAL(clicked()), this, SLOT(ChooseFile()));
    connect(ui->pbParse, SIGNAL(clicked()), this, SLOT(Parse()));
    connect(ui->pbDatetime, SIGNAL(clicked()), this, SLOT(TestDateTime()));
}

void MainWindow::Parse()
{
    QFile file(filename);
    QString line, message;
    QString content;
    bool match = false;
    int matches = 0;
    int lines = 0;
    QDateTime start, end, start2, end2;

    qint64 seconds = 0, seconds2 = 0;

    //pcrecpp::StringPiece str;
    std::string matchstr;

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        HandImporter importer;
        QString parse;
        start2 = QDateTime::currentDateTime();
        std::string newfilename = ui->edtFile->text().toUtf8();

        QTextStream stream(&file);
        parse = stream.readAll();

        importer.setParseContent(parse.toStdString().c_str());
        qDebug() << parse.toUtf8();
        importer.ParseContent();
        //importer.ImportFile(std::string(newfilename.begin(), newfilename.end()));
        end2 = QDateTime::currentDateTime();
    }
    file.close();


    seconds2 = start2.msecsTo(end2);

    message = QString("%1 Zeilen in %2 ms").arg(lines).arg(seconds2);

    QMessageBox::information(this, "Dauer", message, QMessageBox::Ok, QMessageBox::Cancel);
}

void MainWindow::ChooseFile()
{
    QFileDialog filedlg(this);

    filename = filedlg.getOpenFileName();
    ui->edtFile->setText(filename);
}

void MainWindow::TestDateTime()
{
    QDateTime newdate;
    QString datestring;

    newdate.fromString(ui->edtFile->text(), Qt::TextDate);
    newdate = QDateTime::currentDateTime();
    datestring = newdate.toString();
    QMessageBox::information(this, "Datum", datestring);
}
