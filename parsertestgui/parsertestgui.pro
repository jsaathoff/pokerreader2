
#-------------------------------------------------
#
# Project created by QtCreator 2012-08-18T23:21:44
#
#-------------------------------------------------

include(../pokerreaderqtconf.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = parsertestgui
TEMPLATE = app

#DEFINES += PCRE_STATIC
DEFINES += BOOST_ALL_NO_LIB

#CONFIG(debug) : DEFINES += DEBUG
INCLUDEPATH += $${BOOST}
INCLUDEPATH += ../ibpp/


DEFINES += IBPP_WINDOWS
DEFINES += USEREGEX_PARSER

SOURCES += main.cpp\
		mainwindow.cpp \
    ../applib/parser/pokerstarsparser.cpp \
    ../applib/parser/partypokerparser.cpp \
    ../applib/parser/importcontainer.cpp \
    ../applib/parser/handimporter.cpp \
    ../applib/parser/fulltiltparser.cpp \
    ../applib/parser/baseparsertext.cpp \
    ../applib/parser/baseparser.cpp \
    ../applib/datatypes/pokerdefinitions.cpp \
    ../applib/datatypes/hand.cpp \
    ../applib/converter.cpp \
    ../applib/encodingdetector.cpp \
    ../applib/parser/parsehand.cpp \
    ../applib/datatypes/cashhand.cpp
    #../applib/datatypes/parsehand.cpp

HEADERS  += mainwindow.h \
    ../applib/parser/pokerstarsparser.h \
    ../applib/parser/partypokerparser.h \
    ../applib/parser/parserstates.h \
    ../applib/parser/importcontainer.h \
    ../applib/parser/handimporter.h \
    ../applib/parser/fulltiltparser.h \
    ../applib/parser/baseparsertext.h \
    ../applib/parser/baseparser.h \
    ../applib/datatypes/provider.h \
    ../applib/datatypes/pokerdefinitions.h \
    ../applib/datatypes/limittypes.h \
    ../applib/datatypes/handtypes.h \
    ../applib/datatypes/handstates.h \
    ../applib/datatypes/hand.h \
    ../applib/datatypes/currencies.h \
    ../applib/datatypes/actions.h \
    ../applib/converter.h \
    ../applib/encodingdetector.h \
    ../applib/datatypes/playerhandaction.h \
    ../applib/parser/parsehand.h \
    ../applib/datatypes/cashhand.h
    #../applib/datatypes/parsehand.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$${BOOST}/stage/lib/ -lboost_regex-vc100-mt-1_52
else:win32:CONFIG(debug, debug|release): LIBS += -L$${BOOST}/stage/lib/ -lboost_regex-vc100-mt-gd-1_52

win32: LIBS += $${WINLIB}/AdvAPI32.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../lib/ -libpplib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../lib/ -libpplibd

INCLUDEPATH += $$PWD/../ibpp/
DEPENDPATH += $$PWD/../ibpp/

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../lib/ibpplib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../lib/ibpplibd.lib

#INCLUDEPATH += $${CPPLIB}/icu/include


#win32: LIBS += -L$$PWD/../../../Frameworks/CppLibs/icu/lib/ -licuin

#INCLUDEPATH += $$PWD/../../../Frameworks/CppLibs/icu/include
#DEPENDPATH += $$PWD/../../../Frameworks/CppLibs/icu/include
