#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <pcre.h>
//#include <pcrecpp.h>
#include <QRegExp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void connectSignals();

    QString filename;
    
    QRegExp re2;
    
    //pcrecpp::RE * re;

private slots:

    void Parse();

    void ChooseFile();
    
    void TestDateTime();

};

#endif // MAINWINDOW_H
