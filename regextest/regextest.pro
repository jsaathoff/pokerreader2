#-------------------------------------------------
#
# Project created by QtCreator 2012-08-18T23:21:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RegExTest
TEMPLATE = app

DEFINES += PCRE_STATIC


SOURCES += main.cpp\
		mainwindow.cpp \
	../GUI/common/importer.cpp \
	../AppLib/datatypes/cashhand.cpp

HEADERS  += mainwindow.h \
	../AppLib/datatypes/pokerdefinitions.h \
	../GUI/common/importer.h \
	../AppLib/datatypes/cashhand.h

FORMS    += mainwindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../CommonClasses/pcre/lib/release/ -lpcrecpp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../CommonClasses/pcre/lib/debug/ -lpcrecppd

INCLUDEPATH += $$PWD/../../CommonClasses/pcre/lib
DEPENDPATH += $$PWD/../../CommonClasses/pcre/lib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../CommonClasses/pcre/lib/release/pcrecpp.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../CommonClasses/pcre/lib/debug/pcrecppd.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../CommonClasses/pcre/lib/release/ -lpcre
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../CommonClasses/pcre/lib/debug/ -lpcred

INCLUDEPATH += $$PWD/../../CommonClasses/pcre
DEPENDPATH += $$PWD/../../CommonClasses/pcre

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../CommonClasses/pcre/lib/release/pcre.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../CommonClasses/pcre/lib/debug/pcred.lib
